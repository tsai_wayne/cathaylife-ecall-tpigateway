package com.tp.tpigateway.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.tp.tpigateway.vo.gateway.GatewayResponse;

@RestControllerAdvice
public class TPExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(TPExceptionHandler.class);

//    @ExceptionHandler(TPException.class)
//    public TPIGatewayResult handleTPException(TPException e) {
//        log.error("TPException: " + e.getMsg());
//        return TPIGatewayResult.error(e.getMsg());
//    }

//    @ExceptionHandler(CathayApiException.class)
//    public GatewayResponse<?> handleCathayApiException(CathayApiException ex) {
//        log.error(ex.getMessage(), ex);
//        return GatewayResponse.buildFail(ex.getMessage(), ex.getCode());
//    }

    @ExceptionHandler(Exception.class)
    public GatewayResponse<?> handleException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return GatewayResponse.buildFail(ex.getMessage());
    }

}
