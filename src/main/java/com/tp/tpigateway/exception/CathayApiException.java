package com.tp.tpigateway.exception;

public class CathayApiException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    
    private String code; 

    public CathayApiException() {
    }

    public CathayApiException(String msg, String code) {
        super(msg);
        this.code = code;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
