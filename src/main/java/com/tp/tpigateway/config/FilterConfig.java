package com.tp.tpigateway.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tp.tpigateway.filter.HttpRequestFilter;

@Configuration
public class FilterConfig {

//    @Bean
//    public FilterRegistrationBean xssFilterRegistration() {
//        FilterRegistrationBean registration = new FilterRegistrationBean();
//        registration.setDispatcherTypes(DispatcherType.REQUEST);
//        registration.setFilter(new XssFilter());
//        registration.addUrlPatterns("/*");
//        registration.setName("xssFilter");
//        return registration;
//    }

    @Bean
    public FilterRegistrationBean<HttpRequestFilter> httpRequestFilter() {
        FilterRegistrationBean<HttpRequestFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new HttpRequestFilter());
        return registration;
    }
}
