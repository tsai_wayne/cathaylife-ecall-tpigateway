//package com.tp.tpigateway.config;
//
//
//import com.alibaba.druid.filter.config.ConfigTools;
//import com.mongodb.MongoClient;
//import com.mongodb.MongoClientOptions;
//import org.springframework.beans.factory.ObjectProvider;
//import org.springframework.boot.autoconfigure.mongo.MongoClientFactory;
//import org.springframework.boot.autoconfigure.mongo.MongoProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//
//import javax.annotation.PreDestroy;
//
//
//@Configuration
//@EnableConfigurationProperties(MongoProperties.class)
//public class MongoConfig {
//
//    private final MongoClientOptions options;
//
//    private final MongoClientFactory factory;
//
//    private MongoClient mongo;
//
//    public MongoConfig(MongoProperties properties,
//                       ObjectProvider<MongoClientOptions> options, Environment environment) throws Exception {
//        this.options = options.getIfAvailable();
//
//        String encryptPwd = environment.getProperty("mongo-password");
//        String publicKey = environment.getProperty("mongo-public-key");
//        String uri = environment.getProperty("spring.data.mongodb.uri");
//
//        String decryptPwd = ConfigTools.decrypt(publicKey, encryptPwd);
//
//        uri = uri.replace("{{mongo-password}}", decryptPwd);
//
//        properties.setUri(uri);
//
//        this.factory = new MongoClientFactory(properties, environment);
//    }
//
//    @PreDestroy
//    public void close() {
//        if (this.mongo != null) {
//            this.mongo.close();
//        }
//    }
//
//    @Bean
//    public MongoClient mongo() {
//        this.mongo = this.factory.createMongoClient(this.options);
//        return this.mongo;
//    }
//
//    public static void main(String[] args) throws Exception {
//        String password = "TP27714944";
//        String[] arr = ConfigTools.genKeyPair(512);
//        System.out.println("privateKey:" + arr[0]);
//        System.out.println("publicKey:" + arr[1]);
//
//        String encrypt = ConfigTools.encrypt(arr[0], password);
//        System.out.println("password:" + encrypt);
//
//        String decrypt = ConfigTools.decrypt(arr[1], encrypt);
//        System.out.println("decrypt:" + decrypt);
//    }
//
//}
