//package com.tp.tpigateway.config;
//
//import javax.sql.DataSource;
//
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.AutoConfigureBefore;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//import org.springframework.context.annotation.Primary;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import com.alibaba.druid.pool.DruidDataSource;
//import com.alibaba.druid.spring.boot.autoconfigure.properties.DruidStatProperties;
//import com.alibaba.druid.spring.boot.autoconfigure.stat.DruidFilterConfiguration;
//import com.alibaba.druid.spring.boot.autoconfigure.stat.DruidSpringAopConfiguration;
//import com.alibaba.druid.spring.boot.autoconfigure.stat.DruidStatViewServletConfiguration;
//import com.alibaba.druid.spring.boot.autoconfigure.stat.DruidWebStatFilterConfiguration;
//import com.baomidou.mybatisplus.core.MybatisConfiguration;
//import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
//import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
//import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
//
//@Configuration
//@EnableTransactionManagement
//@MapperScan({
//        //"com.tp.tpigateway.common.mybatis.mapper",
//        "com.baomidou.mybatisplus.samples.quickstart.mapper"
//})
//public class MyBatisConfig {
//
//    @Configuration
//    protected static class MybatisPlusConfig {
//        /**
//         * 分页插件
//         */
//        @Bean
//        public PaginationInterceptor paginationInterceptor() {
//            return new PaginationInterceptor();
//        }
//    }
//
//
//    @Configuration
//    @AutoConfigureBefore(DataSourceAutoConfiguration.class)
//    @EnableConfigurationProperties({DruidStatProperties.class, DataSourceProperties.class})
//    @Import({DruidSpringAopConfiguration.class,
//            DruidStatViewServletConfiguration.class,
//            DruidWebStatFilterConfiguration.class,
//            DruidFilterConfiguration.class})
//    public static class DataSourceConfig {
//
//        private static final String THSRC_DATASOURCE_PREFIX = "spring.datasource.druid.thsrc";
//
//        public static final String THSRC_DATASOURCE_BEAN_NAME = "thsrcDataSource";
//
//        @Primary
//        @Bean(name = THSRC_DATASOURCE_BEAN_NAME)
//        @ConfigurationProperties(prefix = THSRC_DATASOURCE_PREFIX)
//        public DruidDataSource thsrc() {
//            return new DruidDataSource();
//        }
//    }
//
//    @Configuration
//    @MapperScan(basePackages = {"com.tp.tpigateway.common.mybatis.mapper.thsrc","com.tp.tpigateway.backend.mapper"}, sqlSessionTemplateRef = "thsrcSqlSessionTemplate")
//    public class ThsrcDataSourceConfig {
//
//        @Autowired
//        @Qualifier(DataSourceConfig.THSRC_DATASOURCE_BEAN_NAME)
//        private DataSource thsrc;
//
//        @Primary
//        @Bean("thsrcSqlSessionFactory")
//        public SqlSessionFactory thsrcSqlSessionFactory() throws Exception {
//            MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
//            factoryBean.setDataSource(thsrc);
//
//            MybatisConfiguration configuration = new MybatisConfiguration();
//            configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
//            configuration.setJdbcTypeForNull(JdbcType.NULL);
//            factoryBean.setConfiguration(configuration);
//            //指定xml路径.
////            factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/mybatis/thsrc/*.xml"));
//
//            return factoryBean.getObject();
//        }
//
//        @Primary
//        @Bean("thsrcSqlSessionTemplate")
//        public SqlSessionTemplate thsrcSqlSessionTemplate() throws Exception {
//            SqlSessionTemplate template = new SqlSessionTemplate(thsrcSqlSessionFactory()); // 使用上面配置的Factory
//            return template;
//        }
//    }
//}
