package com.tp.tpigateway.config;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class TaskExecutorConfig implements AsyncConfigurer {

	private Logger logger = LoggerFactory.getLogger(TaskExecutorConfig.class);

	@Override
	@Bean(name = "baseExecutor")
	@Primary
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		//核心線程數
		taskExecutor.setCorePoolSize(50);
		//最大線程數
		taskExecutor.setMaxPoolSize(100);
		//隊列長度
		taskExecutor.setQueueCapacity(10000);
		//線程名稱前綴
		taskExecutor.setThreadNamePrefix("AsyncTask-");

		//線程池對拒絕任務（無線程可用）的處理策略，目前只支持AbortPolicy、CallerRunsPolicy
		//AbortPolicy:直接拋出java.util.concurrent.RejectedExecutionException異常
		//CallerRunsPolicy:主線程直接執行該任務，執行完之後嘗試添加下一個任務到線程池中，可以有效降低向線程池內添加任務的速度
		taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		taskExecutor.initialize();

		return taskExecutor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {// 異步任務中異常處理
		return (ex, method, params) -> {
            logger.error("=========================="+ex.getMessage()+"=======================");
            logger.debug("", ex);
            logger.error("exception method:" + method.getName());
        };
	}

}
