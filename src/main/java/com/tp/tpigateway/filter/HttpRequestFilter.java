package com.tp.tpigateway.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import com.tp.tpigateway.wrapper.BodyReaderHttpServletRequestWrapper;

import java.io.IOException;

public class HttpRequestFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ServletRequest requestWrapper = null;

        if (request instanceof HttpServletRequest) {
            requestWrapper = new BodyReaderHttpServletRequestWrapper((HttpServletRequest) request);
        }

        if (null == requestWrapper) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(requestWrapper, response);
        }
    }

}