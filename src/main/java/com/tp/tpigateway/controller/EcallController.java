package com.tp.tpigateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.tpigateway.service.EcallService;
import com.tp.tpigateway.vo.flow.FlowRequest;
import com.tp.tpigateway.vo.gateway.ContentAllVipData;
import com.tp.tpigateway.vo.gateway.ContentCheckAmtVoice;
import com.tp.tpigateway.vo.gateway.ContentConfirmVoice;
import com.tp.tpigateway.vo.gateway.ContentGetRgnCode;
import com.tp.tpigateway.vo.gateway.ContentLastIssueData;
import com.tp.tpigateway.vo.gateway.ContentLawAge;
import com.tp.tpigateway.vo.gateway.ContentTempVoice;
import com.tp.tpigateway.vo.gateway.GatewayResponse;

@RestController
@RequestMapping("/ecall")
public class EcallController {

	@Autowired
	private EcallService ecallService;

	@RequestMapping(value = "/law-age", produces = MediaType.APPLICATION_JSON_VALUE)
	public GatewayResponse<ContentLawAge> lawAge(@RequestBody FlowRequest req) {
		return ecallService.lawAge(req);
	}

	@RequestMapping(value = "/last-issue-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentLastIssueData> lastIssueData(@RequestBody FlowRequest req) {
        return ecallService.lastIssueData(req);
    }

	@RequestMapping(value = "/all-vip-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentAllVipData> allVipData(@RequestBody FlowRequest req) {
        return ecallService.allVipData(req);
    }

	@RequestMapping(value = "/do-confirm-voice", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentConfirmVoice> doConfirmVoice(@RequestBody FlowRequest req) {
        return ecallService.doConfirmVoice(req);
    }

	@RequestMapping(value = "/insert-temp-voice", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentTempVoice> insertTempVoice(@RequestBody FlowRequest req) {
        return ecallService.insertTempVoice(req);
    }

	@RequestMapping(value = "/do-checkamt-voice", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(@RequestBody FlowRequest req) {
        return ecallService.doCheckAmtVoice(req);
    }

	@RequestMapping(value = "/get-rgn-code", produces = MediaType.APPLICATION_JSON_VALUE)
    public GatewayResponse<ContentGetRgnCode> getRgnCode(@RequestBody FlowRequest req) {
        return ecallService.getRgnCode(req);
    }

}
