package com.tp.tpigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpigatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpigatewayApplication.class, args);
	}

}
