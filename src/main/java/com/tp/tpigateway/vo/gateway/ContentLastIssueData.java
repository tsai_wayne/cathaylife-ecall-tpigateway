package com.tp.tpigateway.vo.gateway;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * == 取得上一次投保紀錄 ==
 * TPIGateway 回傳至 ChatFlow JSON 的 content 物件
 *
 * "content": {
 *     "RTN_CODE": "000",
 *     "LOCAL_MAIN": "100",
 *     "LOCAL_MEDICAL": "0",
 *     "OVERSEA_MAIN": "100",
 *     "OVERSEA_MEDICAL": "10",
 *     "OVERSEA_ILLNESS": "20",
 *     "OVERSEA_ILLNESS_TYPE": "3"
 * }
 *
 * @see GatewayResponse
 */
public class ContentLastIssueData {

	@JsonProperty("RTN_CODE")
    private String returnCode;
	@JsonProperty("LOCAL_MAIN")
    private String localMain;
	@JsonProperty("LOCAL_MEDICAL")
    private String localMedical;
	@JsonProperty("OVERSEA_MAIN")
    private String overseaMain;
	@JsonProperty("OVERSEA_MEDICAL")
    private String overseaMedical;
	@JsonProperty("OVERSEA_ILLNESS")
    private String overseaIllness;
	@JsonProperty("OVERSEA_ILLNESS_TYPE")
    private String overseaIllnessType;
	
	public ContentLastIssueData() {
	}

	public ContentLastIssueData(String rtnCode, String localMain, String localMedical, String overseaMain,
			String overseaMedical, String overseaIllness, String overseaIllnessType) {
		super();
		this.returnCode = rtnCode;
		this.localMain = localMain;
		this.localMedical = localMedical;
		this.overseaMain = overseaMain;
		this.overseaMedical = overseaMedical;
		this.overseaIllness = overseaIllness;
		this.overseaIllnessType = overseaIllnessType;
	}
	
	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getLocalMain() {
		return localMain;
	}

	public void setLocalMain(String localMain) {
		this.localMain = localMain;
	}

	public String getLocalMedical() {
		return localMedical;
	}

	public void setLocalMedical(String localMedical) {
		this.localMedical = localMedical;
	}

	public String getOverseaMain() {
		return overseaMain;
	}

	public void setOverseaMain(String overseaMain) {
		this.overseaMain = overseaMain;
	}

	public String getOverseaMedical() {
		return overseaMedical;
	}

	public void setOverseaMedical(String overseaMedical) {
		this.overseaMedical = overseaMedical;
	}

	public String getOverseaIllness() {
		return overseaIllness;
	}

	public void setOverseaIllness(String overseaIllness) {
		this.overseaIllness = overseaIllness;
	}

	public String getOverseaIllnessType() {
		return overseaIllnessType;
	}

	public void setOverseaIllnessType(String overseaIllnessType) {
		this.overseaIllnessType = overseaIllnessType;
	}
}
