package com.tp.tpigateway.vo.gateway;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 *     "RTN_CODE": "000",
 *     "RGN_PLACE" : "長灘島",
 *     "RGN_NAME": "菲律賓"
 * }
 *
 * {
 *     "RTN_CODE": "999",
 *     "RGN_PLACE" : "",
 *     "RGN_NAME": ""
 * }
 */
public class ContentGetRgnCode {

    @JsonProperty("RTN_CODE")
    private String rtnCode;
    @JsonProperty("RGN_PLACE")
    private String rgnPlace;
    @JsonProperty("RGN_NAME")
    private String rgnName;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRgnPlace() {
        return rgnPlace;
    }

    public void setRgnPlace(String rgnPlace) {
        this.rgnPlace = rgnPlace;
    }

    public String getRgnName() {
        return rgnName;
    }

    public void setRgnName(String rgnName) {
        this.rgnName = rgnName;
    }

}
