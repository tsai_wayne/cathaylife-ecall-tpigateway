package com.tp.tpigateway.vo.gateway;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * TPIGateway 回傳至 Flow 的 JSON 結構，各 API 的差異部份為 "content" 物件的內容
 *
 * ResponseBase 為 JSON 相同部份，將會變動的 content 使用泛型的方式，達到動態產生 JSON
 *
 * {
 *     "msg": "success",
 *     "code": 0,
 *     "data": {
 *         "from": "bot",
 *         "showtime": "2020-03-05 11:40:20",
 *         "content": {
 *             ...
 *         },
 *         "channel": "cathaylife",
 *         "source": "cathaylife",
 *         "role": "cathaylife",
 *         "fb": false,
 *         "fbIntent": null,
 *         "sbIntent": null
 *     }
 * }
 *
 * @param <T>
 */
public class GatewayResponse<T> {

    private static final String CATHAY_LIFE = "cathaylife";
    private static final String FROM_BOT = "bot";
    private static final int CODE_SUCCESS = 0;
    private static final int CODE_FAIL = -1;

    private static final DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private String msg;
    private int code;
    private Data<T> data;


    private GatewayResponse() {
    }
    private GatewayResponse(int code) {
    }
    private GatewayResponse(int code, String msg) {
        this.code =  code;
    	this.msg = msg;
    }

    public static <T> GatewayResponse<T> buildSuccess(Class<T> clazz) {
        return new GatewayResponse<T>(CODE_SUCCESS);
    }

    public static <T> GatewayResponse<T> buildSuccess(Class<T> clazz, String msg) {
        return new GatewayResponse<T>(CODE_SUCCESS, msg);
    }

    public static <T> GatewayResponse<T> buildFail(Class<T> clazz) {
        return new GatewayResponse<T>(CODE_FAIL);
    }

    public static GatewayResponse<?> buildFail(String msg) {
        return new GatewayResponse<>(CODE_FAIL, msg);
    }

    public static <T> GatewayResponse<T> buildFail(Class<T> clazz, String msg) {
        return new GatewayResponse<T>(CODE_FAIL, msg);
    }

    public static <T> GatewayResponse.Data<T> buildDefaultData() {
        return new GatewayResponse.Data<T>();
    }
    public static <T> GatewayResponse.Data<T> buildData(String channel, String source, String role, String from) {
        return new GatewayResponse.Data<T>(channel, source, role, from);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data<T> getData() {
        return data;
    }

    public void setData(Data<T> data) {
        this.data = data;
    }

    /**
     * TPIGateway 回傳至 ChatFlow 的 JSON 的 data 物件
     *
     * @see GatewayResponse
     */
    public static class Data<T> {
        private String channel;
        private String source;
        private String role;
        private boolean fb;
        private String fbIntent;
        private String sbIntent;
        private String from;
        private String showtime;
        private T content;

        private Data() {
            this(CATHAY_LIFE, CATHAY_LIFE, CATHAY_LIFE, FROM_BOT);
        }
        private Data(String channel, String source, String role, String from) {
            this.channel = channel;
            this.source = source;
            this.role = role;
            this.from = from;
            this.fb = false;
            this.showtime = LocalDateTime.now().format(DT_FORMAT);
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public boolean getFb() {
            return fb;
        }

        public void setFb(boolean fb) {
            this.fb = fb;
        }

        public String getFbIntent() {
            return fbIntent;
        }

        public void setFbIntent(String fbIntent) {
            this.fbIntent = fbIntent;
        }

        public String getSbIntent() {
            return sbIntent;
        }

        public void setSbIntent(String sbIntent) {
            this.sbIntent = sbIntent;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getShowtime() {
            return showtime;
        }

        public void setShowtime(String showtime) {
            this.showtime = showtime;
        }

        public T getContent() {
            return content;
        }

        public void setContent(T content) {
            this.content = content;
        }
    }
}
