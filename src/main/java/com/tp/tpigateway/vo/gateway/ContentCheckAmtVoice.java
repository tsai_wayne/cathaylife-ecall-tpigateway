package com.tp.tpigateway.vo.gateway;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tp.tpigateway.vo.cathay.CheckAmtVoiceData;
import com.tp.tpigateway.vo.cathay.CheckAmtVoiceData.InsdList;

/**
 * "content": {
 *     "RTN_CODE": "000",
 *     "RGN_CODE": "3",
 *     "ISSUE_DATE" : "2020-02-20",
 *     "BGN_TIME" : "08:00:00"，
 *     "ISSUE_DAYS": "3",
 *     "APLY_NO": "0010700001",
 *     "TOT_PREM": "2000",
 *     "INSD_LIST": [
 *         {
 *             "INSD_ID": "A123456789",
 *             "INSD_BRDY": "1990-07-29",
 *             "HD_AMT": "1000",
 *             "HP_AMT": "100",
 *             "HK_TYPE": "1",
 *             "HK_AMT": "10",
 *             "OMTP_AMT": "2",
 *             "TRVL_NOT_CONV": "C",
 *             "SELF_PREM": "1000"
 *         }
 *     ]
 * },
 */
public class ContentCheckAmtVoice {
    @JsonProperty("RTN_CODE")
    private String returnCode;
    @JsonProperty("RGN_CODE")
    private String rgnCode;
    @JsonProperty("ISSUE_DATE")
    private String issueDate;
    @JsonProperty("BGN_TIME")
    private String bgnTime;
    @JsonProperty("ISSUE_DAYS")
    private String issueDays;
    @JsonProperty("APLY_NO")
    private String aplyNo;
    @JsonProperty("TOT_PREM")
    private String totPrem;
    @JsonProperty("INSD_LIST")
    private List<InsdList> insdList;

    public ContentCheckAmtVoice() {
    }
    public ContentCheckAmtVoice(String rtnCode, CheckAmtVoiceData data) {
        this.returnCode = rtnCode;
        this.aplyNo = data.getAplyNo();
        this.issueDate = data.getIssueDate();
        this.bgnTime = data.getBgnTime();
        this.issueDays = data.getIssueDays();
        this.rgnCode = data.getRgnCode();
        this.insdList = data.getInsdList();
    }


    public String getReturnCode() {
        return returnCode;
    }
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    public String getAplyNo() {
        return aplyNo;
    }
    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }
    public String getIssueDate() {
        return issueDate;
    }
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    public String getBgnTime() {
        return bgnTime;
    }
    public void setBgnTime(String bgnTime) {
        this.bgnTime = bgnTime;
    }
    public String getIssueDays() {
        return issueDays;
    }
    public void setIssueDays(String issueDays) {
        this.issueDays = issueDays;
    }
    public String getRgnCode() {
        return rgnCode;
    }
    public void setRgnCode(String rgnCode) {
        this.rgnCode = rgnCode;
    }
    public List<InsdList> getInsdList() {
        return insdList;
    }
    public void setInsdList(List<InsdList> insdList) {
        this.insdList = insdList;
    }

}
