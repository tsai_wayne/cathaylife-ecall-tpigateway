package com.tp.tpigateway.vo.gateway;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tp.tpigateway.vo.cathay.ConfirmVoiceData;

/**
 * "content": {
 *     "RTN_CODE":"000",
 *     "APLY_NO":"0010220330"
 * }
 */
public class ContentConfirmVoice {

    @JsonProperty("RTN_CODE")
    private String returnCode;
    @JsonProperty("APLY_NO")
    private String aplyNo;

    public ContentConfirmVoice() {
    }
    public ContentConfirmVoice(String returnCode, ConfirmVoiceData data) {
        this.returnCode = returnCode;
        this.aplyNo = data.getAplyNo();
    }


    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getAplyNo() {
        return aplyNo;
    }

    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }

}
