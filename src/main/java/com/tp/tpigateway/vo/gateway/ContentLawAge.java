package com.tp.tpigateway.vo.gateway;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 *     "RTN_CODE": "000",
 *     "AGE": [12, 50],
 *     "ISSUE_DATE": "2020-02-20",
 *     "VIP_BRDY": ["1970-01-01", "1980-08-08"]
 * }
 */
public class ContentLawAge {

    @JsonProperty("RTN_CODE")
    private String returnCode;
    @JsonProperty("AGE")
    private List<String> age;
    @JsonProperty("ISSUE_DATE")
    private String issueDate;
    @JsonProperty("VIP_BRDY")
    private List<String> vipBirthday;

    public ContentLawAge() {
    }

    public ContentLawAge(String returnCode, List<String> age, String issueDate, List<String> vipBirthday) {
        this.returnCode = returnCode;
        this.age = age;
        this.issueDate = issueDate;
        this.vipBirthday = vipBirthday;
    }


    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public List<String> getAge() {
        return age;
    }

    public void setAge(List<String> age) {
        this.age = age;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public List<String> getVipBirthday() {
        return vipBirthday;
    }

    public void setVipBirthday(List<String> vipBirthday) {
        this.vipBirthday = vipBirthday;
    }

}
