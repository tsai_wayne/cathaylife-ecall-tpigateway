package com.tp.tpigateway.vo.gateway;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.tp.tpigateway.vo.cathay.AllVipData.MainMember;
import com.tp.tpigateway.vo.cathay.AllVipData.SubMember;

/**
* == 整戶資料查詢 ==
* "content": {
*   "RTN_CODE": "000",
*   "checkAGNT_ID": "00",
*   "PROD_ID": "Y",
*   "TRVL_PROD_ID": "M",
*   "onlyVIP": "N",
*   "newCASE_NO": "Y",
*   "newCASE_NO_INS": "Y",
*   "notWelcome": "00",
*   "a190_bo": {主會員 Mainmember
*   },
*   "a191_boList": [從屬會員 SubMember
*   ]
* }
 * @see GatewayResponse
 * @see MainMember
 * @see SubMember
 */
@JsonInclude(Include.NON_NULL)
public class ContentAllVipData {

	@JsonProperty("RTN_CODE")
    private String returnCode;

	@JsonProperty("checkAGNT_ID")
	private String checkAgntId;

	@JsonProperty("PROD_ID")
	private String prodId;

	@JsonProperty("TRVL_PROD_ID")
	private String trvlProdId;

	@JsonProperty("onlyVIP")
	private String onlyVip;

	@JsonProperty("newCASE_NO")
	private String newCaseNo;

	@JsonProperty("newCASE_NO_INS")
	private String newCaseNoIns;

	@JsonProperty("notWelcome")
	private String notWelcome;

	@JsonProperty("a190_bo")
	private MainMember mainMember;

	@JsonProperty("a191_boList")
	private List<SubMember> subMemberList;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getCheckAgntId() {
		return checkAgntId;
	}

	public void setCheckAgntId(String checkAgntId) {
		this.checkAgntId = checkAgntId;
	}

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public String getTrvlProdId() {
		return trvlProdId;
	}

	public void setTrvlProdId(String trvlProdId) {
		this.trvlProdId = trvlProdId;
	}

	public String getOnlyVip() {
		return onlyVip;
	}

	public void setOnlyVip(String onlyVip) {
		this.onlyVip = onlyVip;
	}

	public String getNewCaseNo() {
		return newCaseNo;
	}

	public void setNewCaseNo(String newCaseNo) {
		this.newCaseNo = newCaseNo;
	}

	public String getNewCaseNoIns() {
		return newCaseNoIns;
	}

	public void setNewCaseNoIns(String newCaseNoIns) {
		this.newCaseNoIns = newCaseNoIns;
	}

	public String getNotWelcome() {
		return notWelcome;
	}

	public void setNotWelcome(String notWelcome) {
		this.notWelcome = notWelcome;
	}

	public MainMember getMainMember() {
		return mainMember;
	}

	public void setMainMember(MainMember mainMember) {
		this.mainMember = mainMember;
	}

	public List<SubMember> getSubMemberList() {
		return subMemberList;
	}

	public void setSubMemberList(List<SubMember> subMemberList) {
		this.subMemberList = subMemberList;
	}

}
