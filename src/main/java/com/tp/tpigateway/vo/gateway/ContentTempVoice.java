package com.tp.tpigateway.vo.gateway;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tp.tpigateway.vo.cathay.TempVoiceData;
import com.tp.tpigateway.vo.cathay.TempVoiceData.InsdList;

/**
 * "content": {
 *     "RTN_CODE": "111",
 *     "APLY_NO": "",
 *     "ISSUE_DATE" : "2020-03-20",
 *     "BGN_TIME" : "08:00:00",
 *     "ISSUE_DAYS" : "3",
 *     "RGN_CODE" : "3",
 *     "INSD_LIST" : [
 *         {
 *             "INSD_ID": "A123456789",
 *             "INSD_BRDY": "1990-07-29",
 *             "HD_AMT": "1000",
 *             "HP_AMT": "100",
 *             "HK_TYPE": "1",
 *             "HK_AMT": "10",
 *             "OMTP_AMT": "2",
 *             "TRVL_NOT_CONV": "C"
 *         }
 *     ]
 * }
 */
public class ContentTempVoice {

    @JsonProperty("RTN_CODE")
    private String returnCode;
    @JsonProperty("APLY_NO")
    private String aplyNo;
    @JsonProperty("ISSUE_DATE")
    private String issueDate;
    @JsonProperty("BGN_TIME")
    private String bgnTime;
    @JsonProperty("ISSUE_DAYS")
    private String issueDays;
    @JsonProperty("RGN_CODE")
    private String rgnCode;
    @JsonProperty("INSD_LIST")
    private List<InsdList> insdList;

    public ContentTempVoice() {
    }
    public ContentTempVoice(String rtnCode, TempVoiceData data) {
        this.returnCode = rtnCode;
        this.aplyNo = data.getAplyNo();
        this.issueDate = data.getIssueDate();
        this.bgnTime = data.getBgnTime();
        this.issueDays = data.getIssueDays();
        this.rgnCode = data.getRgnCode();
        this.insdList = data.getInsdList();
    }


    public String getReturnCode() {
        return returnCode;
    }
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    public String getAplyNo() {
        return aplyNo;
    }
    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }
    public String getIssueDate() {
        return issueDate;
    }
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    public String getBgnTime() {
        return bgnTime;
    }
    public void setBgnTime(String bgnTime) {
        this.bgnTime = bgnTime;
    }
    public String getIssueDays() {
        return issueDays;
    }
    public void setIssueDays(String issueDays) {
        this.issueDays = issueDays;
    }
    public String getRgnCode() {
        return rgnCode;
    }
    public void setRgnCode(String rgnCode) {
        this.rgnCode = rgnCode;
    }
    public List<InsdList> getInsdList() {
        return insdList;
    }
    public void setInsdList(List<InsdList> insdList) {
        this.insdList = insdList;
    }

}
