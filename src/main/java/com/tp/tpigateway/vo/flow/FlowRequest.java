package com.tp.tpigateway.vo.flow;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * request from Flow to TPIGateway
 *
 * /ecall/law-age
 *   {
 *       "channel": "cathaylife",
 *       "source": "cathaylife",
 *       "role": "cathaylife",
 *       "fbIntent": "",
 *       "sbIntent": "",
 *       "issueDate": "2020-02-20",
 *       "vipBrdy": ["1970-01-01", "1980-08-08"]
 *   }
 *
 * /ecall/last-issue-data
 *   {
 *       "channel": "cathaylife",
 *       "source": "cathaylife",
 *       "role": "cathaylife",
 *       "fbIntent": "",
 *       "sbIntent": "",
 *       "vipId" : "A29091346D"
 *   }
 *
 * ...
 *
 * /ecall/get-rgn-code
 *   {
 *       "channel": "cathaylife",
 *       "source": "cathaylife",
 *       "role": "cathaylife",
 *       "fbIntent": "",
 *       "sbIntent": "",
 *       "origStr": "泰國",
 *       "places": [
 *           {
 *               "match_user_phrase": false,
 *               "start": 0,
 *               "entity_id": 3685,
 *               "info": "東南亞-泰國;Thailand",
 *               "end": 2,
 *               "tokenized_text": "泰 國",
 *               "text": "泰國",
 *               "is_builtin": true,
 *               "key_user_phrase": null,
 *               "entity": "sys.世界國家城市"
 *           }
 *       ]
 *   }
 *
 * @author Wayne Tsai
 * @author Tony Liu
 */
public class FlowRequest {

    private String channel;
    private String source;
    private String role;
    private String fbIntent;
    private String sbIntent;

    private String issueDate;
    private List<String> vipBrdy;
    private String vipId;
    private String aplyNo;
    private String bgnTime;
    private String issueDays;
    private String rgnCode;
    private String origStr;
    private List<InsdList> insdList;
    private List<Places> places;


    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFbIntent() {
        return fbIntent;
    }

    public void setFbIntent(String fbIntent) {
        this.fbIntent = fbIntent;
    }

    public String getSbIntent() {
        return sbIntent;
    }

    public void setSbIntent(String sbIntent) {
        this.sbIntent = sbIntent;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public List<String> getVipBrdy() {
        return vipBrdy;
    }

    public void setVipBrdy(List<String> vipBrdy) {
        this.vipBrdy = vipBrdy;
    }

    public String getVipId() {
        return vipId;
    }

    public void setVipId(String vipId) {
        this.vipId = vipId;
    }

    public String getAplyNo() {
        return aplyNo;
    }

    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }

    public String getOrigStr() {
        return origStr;
    }

    public void setOrigStr(String origStr) {
        this.origStr = origStr;
    }

    public String getBgnTime() {
        return bgnTime;
    }

    public void setBgnTime(String bgnTime) {
        this.bgnTime = bgnTime;
    }

    public String getIssueDays() {
        return issueDays;
    }

    public void setIssueDays(String issueDays) {
        this.issueDays = issueDays;
    }

    public String getRgnCode() {
        return rgnCode;
    }

    public void setRgnCode(String rgnCode) {
        this.rgnCode = rgnCode;
    }

    public List<Places> getPlaces() {
        return places;
    }

    public void setPlaces(List<Places> places) {
        this.places = places;
    }

    public List<InsdList> getInsdList() {
        return insdList;
    }

    public void setInsdList(List<InsdList> insdList) {
        this.insdList = insdList;
    }


    public static class Places {
        @JsonProperty(value = "match_user_phrase")
        private String matchUserPhrase;
        private String start;
        @JsonProperty(value = "entity_id")
        private String entityId;
        private String info;
        private String end;
        @JsonProperty(value = "tokenized_text")
        private String tokenizedText;
        private String text;
        @JsonProperty(value = "is_builtin")
        private String isBuiltin;
        @JsonProperty(value = "key_user_phrase")
        private String keyUserPhrase;
        private String entity;

        public String getMatchUserPhrase() {
            return matchUserPhrase;
        }
        public void setMatchUserPhrase(String matchUserPhrase) {
            this.matchUserPhrase = matchUserPhrase;
        }
        public String getStart() {
            return start;
        }
        public void setStart(String start) {
            this.start = start;
        }
        public String getEntityId() {
            return entityId;
        }
        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }
        public String getInfo() {
            return info;
        }
        public void setInfo(String info) {
            this.info = info;
        }
        public String getEnd() {
            return end;
        }
        public void setEnd(String end) {
            this.end = end;
        }
        public String getTokenizedText() {
            return tokenizedText;
        }
        public void setTokenizedText(String tokenizedText) {
            this.tokenizedText = tokenizedText;
        }
        public String getText() {
            return text;
        }
        public void setText(String text) {
            this.text = text;
        }
        public String getIsBuiltin() {
            return isBuiltin;
        }
        public void setIsBuiltin(String isBuiltin) {
            this.isBuiltin = isBuiltin;
        }
        public String getKeyUserPhrase() {
            return keyUserPhrase;
        }
        public void setKeyUserPhrase(String keyUserPhrase) {
            this.keyUserPhrase = keyUserPhrase;
        }
        public String getEntity() {
            return entity;
        }
        public void setEntity(String entity) {
            this.entity = entity;
        }
    }

    public static class InsdList {
        private String insdId;
        private String insdBrdy;
        private String hdAmt;
        private String hpAmt;
        private String hkType;
        private String hkAmt;
        private String omtpAmt;
        private String trvlNotConv;

        public String getInsdId() {
            return insdId;
        }
        public void setInsdId(String insdId) {
            this.insdId = insdId;
        }
        public String getInsdBrdy() {
            return insdBrdy;
        }
        public void setInsdBrdy(String insdBrdy) {
            this.insdBrdy = insdBrdy;
        }
        public String getHdAmt() {
            return hdAmt;
        }
        public void setHdAmt(String hdAmt) {
            this.hdAmt = hdAmt;
        }
        public String getHpAmt() {
            return hpAmt;
        }
        public void setHpAmt(String hpAmt) {
            this.hpAmt = hpAmt;
        }
        public String getHkType() {
            return hkType;
        }
        public void setHkType(String hkType) {
            this.hkType = hkType;
        }
        public String getHkAmt() {
            return hkAmt;
        }
        public void setHkAmt(String hkAmt) {
            this.hkAmt = hkAmt;
        }
        public String getOmtpAmt() {
            return omtpAmt;
        }
        public void setOmtpAmt(String omtpAmt) {
            this.omtpAmt = omtpAmt;
        }
        public String getTrvlNotConv() {
            return trvlNotConv;
        }
        public void setTrvlNotConv(String trvlNotConv) {
            this.trvlNotConv = trvlNotConv;
        }
    }

}
