package com.tp.tpigateway.vo.cathay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 *     "APLY_NO": "0010700001"
 * }
 */
public class ConfirmVoiceData {

    @JsonProperty("APLY_NO")
    private String aplyNo;

    public String getAplyNo() {
        return aplyNo;
    }

    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }

}
