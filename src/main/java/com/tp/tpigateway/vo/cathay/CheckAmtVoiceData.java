package com.tp.tpigateway.vo.cathay;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * "data": {
 *     "RGN_CODE":"10",
 *     "ISSUE_DATE":"2020-03-20",
 *     "ISSUE_DAYS":"3",
 *     "APLY_NO":"0010700001",
 *     "INSD_LIST":[
 *         {
 *             "INSD_ID":"N26648942K",
 *             "INSD_BRDY":"1990-07-29",
 *             "HD_AMT":"100",
 *             "HP_AMT":"10",
 *             "HK_TYPE":"3",
 *             "HK_AMT":"10",
 *             "OMTP_AMT":"1",
 *             "TRVL_NOT_CONV":"N",
 *             "SELF_PREM":"1000"
 *         },
 *         ...
 *     ],
 *     "SRC":"G",
 *     "BGN_TIME":"08:00:00",
 *     "TOT_PREM":"2000"
 * }
 */
public class CheckAmtVoiceData {

    @JsonProperty("RTN_CODE")
    private String returnCode;
    @JsonProperty("APLY_NO")
    private String aplyNo;
    @JsonProperty("ISSUE_DATE")
    private String issueDate;
    @JsonProperty("BGN_TIME")
    private String bgnTime;
    @JsonProperty("ISSUE_DAYS")
    private String issueDays;
    @JsonProperty("RGN_CODE")
    private String rgnCode;
    @JsonProperty("INSD_LIST")
    private List<InsdList> insdList;

    public String getReturnCode() {
        return returnCode;
    }
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    public String getAplyNo() {
        return aplyNo;
    }
    public void setAplyNo(String aplyNo) {
        this.aplyNo = aplyNo;
    }
    public String getIssueDate() {
        return issueDate;
    }
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }
    public String getBgnTime() {
        return bgnTime;
    }
    public void setBgnTime(String bgnTime) {
        this.bgnTime = bgnTime;
    }
    public String getIssueDays() {
        return issueDays;
    }
    public void setIssueDays(String issueDays) {
        this.issueDays = issueDays;
    }
    public String getRgnCode() {
        return rgnCode;
    }
    public void setRgnCode(String rgnCode) {
        this.rgnCode = rgnCode;
    }
    public List<InsdList> getInsdList() {
        return insdList;
    }
    public void setInsdList(List<InsdList> insdList) {
        this.insdList = insdList;
    }


    public static class InsdList {
        @JsonProperty("INSD_ID")
        private String insdId;
        @JsonProperty("INSD_BRDY")
        private String insdBrdy;
        @JsonProperty("HD_AMT")
        private String hdAmt;
        @JsonProperty("HP_AMT")
        private String hpAmt;
        @JsonProperty("HK_TYPE")
        private String hkType;
        @JsonProperty("HK_AMT")
        private String hkAmt;
        @JsonProperty("OMTP_AMT")
        private String omtpAmt;
        @JsonProperty("TRVL_NOT_CONV")
        private String trvlNotConv;
        @JsonProperty("SELF_PREM")
        private String selfPrem;

        public String getInsdId() {
            return insdId;
        }
        public void setInsdId(String insdId) {
            this.insdId = insdId;
        }
        public String getInsdBrdy() {
            return insdBrdy;
        }
        public void setInsdBrdy(String insdBrdy) {
            this.insdBrdy = insdBrdy;
        }
        public String getHdAmt() {
            return hdAmt;
        }
        public void setHdAmt(String hdAmt) {
            this.hdAmt = hdAmt;
        }
        public String getHpAmt() {
            return hpAmt;
        }
        public void setHpAmt(String hpAmt) {
            this.hpAmt = hpAmt;
        }
        public String getHkType() {
            return hkType;
        }
        public void setHkType(String hkType) {
            this.hkType = hkType;
        }
        public String getHkAmt() {
            return hkAmt;
        }
        public void setHkAmt(String hkAmt) {
            this.hkAmt = hkAmt;
        }
        public String getOmtpAmt() {
            return omtpAmt;
        }
        public void setOmtpAmt(String omtpAmt) {
            this.omtpAmt = omtpAmt;
        }
        public String getTrvlNotConv() {
            return trvlNotConv;
        }
        public void setTrvlNotConv(String trvlNotConv) {
            this.trvlNotConv = trvlNotConv;
        }
        public String getSelfPrem() {
            return selfPrem;
        }
        public void setSelfPrem(String selfPrem) {
            this.selfPrem = selfPrem;
        }
    }

}
