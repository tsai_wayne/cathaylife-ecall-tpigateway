package com.tp.tpigateway.vo.cathay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 國泰 易call保 API 的 JSON 回傳結構如下，各 API 差異為 detail > data 的部份
 *
 * CathayBaseResponse 為 JSON 相同部份，將會變動的 data 使用泛型的方式，達到動態產生 JSON
 *
 *
 * {
 *     "returnCode": "0",
 *     "returnMsg": "API success",
 *     "UUID": "CB_API1331_20200221_DEC228F4C2_T01",
 *     "hostname": "cxlcsxat01"
 *     "detail": {
 *         "resultMsg": "傳入參數:會員生日必須為西元日期",
 *         "shortMsg": "未通過輸入檢核",
 *         "resultCode": "097",
 *         "data": {
 *             ...
 *         },
 *         "returnMessage": {
 *             "m_desc": [
 *                 "傳入參數: 會員生日必須為西元日期"
 *             ],
 *             "m_msgid": "",
 *             "m_sysid": "",
 *             "m_type": "",
 *             "m_url": "",
 *             "m_idx": 0,
 *             "m_code": -12
 *         }
 *     },
 * }
 *
 * @param <T>
 */
public class CathayBaseResponse<T> {

	private String returnCode;
	private String returnMsg;

	@JsonProperty("UUID")
	private String uuid;
	private String hostname;
	private CathayResponseDetail<T> detail;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public CathayResponseDetail<T> getDetail() {
		return detail;
	}

	public void setDetail(CathayResponseDetail<T> detail) {
		this.detail = detail;
	}

}
