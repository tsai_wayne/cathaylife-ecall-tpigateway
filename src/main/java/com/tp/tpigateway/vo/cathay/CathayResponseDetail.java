package com.tp.tpigateway.vo.cathay;

/**
 * 國泰 易call保 API response JSON 的 detail 物件
 * @author Wayne Tsai
 *
 * @param <T>
 */
public class CathayResponseDetail<T> {

	private String resultMsg;
	private String shortMsg;
	private String resultCode;
	private T data;
	private CathayResponseReturnMessage returnMessage;

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getShortMsg() {
		return shortMsg;
	}

	public void setShortMsg(String shortMsg) {
		this.shortMsg = shortMsg;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public CathayResponseReturnMessage getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(CathayResponseReturnMessage returnMessage) {
		this.returnMessage = returnMessage;
	}

}