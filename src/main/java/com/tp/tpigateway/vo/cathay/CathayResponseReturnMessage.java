package com.tp.tpigateway.vo.cathay;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 國泰 易call保 API response JSON 的 returnMessage 物件
 * @author Wayne Tsai
 *
 */
public class CathayResponseReturnMessage {
	
	@JsonProperty("m_desc")
	private List<String> mDesc;
	
	@JsonProperty("m_msgid")
	private String mMsgid;
	
	@JsonProperty("m_sysid")
	private String mSysid;
	
	@JsonProperty("m_type")
	private String mType;
	
	@JsonProperty("m_url")
	private String mUrl;
	
	@JsonProperty("m_idx")
	private String mIdx;
	
	@JsonProperty("m_code")
	private String mCode;

	public List<String> getmDesc() {
		return mDesc;
	}

	public void setmDesc(List<String> mDesc) {
		this.mDesc = mDesc;
	}

	public String getmMsgid() {
		return mMsgid;
	}

	public void setmMsgid(String mMsgid) {
		this.mMsgid = mMsgid;
	}

	public String getmSysid() {
		return mSysid;
	}

	public void setmSysid(String mSysid) {
		this.mSysid = mSysid;
	}

	public String getmType() {
		return mType;
	}

	public void setmType(String mType) {
		this.mType = mType;
	}

	public String getmUrl() {
		return mUrl;
	}

	public void setmUrl(String mUrl) {
		this.mUrl = mUrl;
	}

	public String getmIdx() {
		return mIdx;
	}

	public void setmIdx(String mIdx) {
		this.mIdx = mIdx;
	}

	public String getmCode() {
		return mCode;
	}

	public void setmCode(String mCode) {
		this.mCode = mCode;
	}

}