package com.tp.tpigateway.vo.cathay;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * == 整戶資料查詢  ==
 * 國泰 易call保 API response JSON 的 data 物件
 * "data":{
 *   "checkAGNT_ID":"00",
 *   "onlyVIP":"N",
 *   "newCASE_NO":"N",
 *   "a191_boList":[從屬會員陣列，見下方 SubMember物件
 *   ],
 *   "a190_bo":{主會員，見下方 MainMember物件
 *   },
 *   "PROD_ID":"Y",
 *   "notWelcome":"00",
 *   "newCASE_NO_INS":"N",
 *   "TRVL_PROD_ID":"N"
 * }
 * @see MainMember
 * @see SubMember
 */
public class AllVipData {

	@JsonProperty("checkAGNT_ID")
	private String checkAgntId;

	@JsonProperty("onlyVIP")
	private String onlyVip;

	@JsonProperty("a191_boList")
	private List<SubMember> subMemberList;

	@JsonProperty("newCASE_NO")
	private String newCaseNo;

	@JsonProperty("a190_bo")
	private MainMember mainMember;

	@JsonProperty("VIP_ID")
    private String vipId;

	@JsonProperty("PROD_ID")
	private String prodId;

	@JsonProperty("notWelcome")
	private String notWelcome;

	@JsonProperty("newCASE_NO_INS")
	private String newCaseNoIns;

	@JsonProperty("TRVL_PROD_ID")
	private String trvlProdId;

	public String getCheckAgntId() {
		return checkAgntId;
	}

	public void setCheckAgntId(String checkAgntId) {
		this.checkAgntId = checkAgntId;
	}

	public String getOnlyVip() {
		return onlyVip;
	}

	public void setOnlyVip(String onlyVip) {
		this.onlyVip = onlyVip;
	}

	public List<SubMember> getSubMemberList() {
		return subMemberList;
	}

	public void setSubMemberList(List<SubMember> subMemberList) {
		this.subMemberList = subMemberList;
	}

	public String getNewCaseNo() {
		return newCaseNo;
	}

	public void setNewCaseNo(String newCaseNo) {
		this.newCaseNo = newCaseNo;
	}

	public MainMember getMainMember() {
		return mainMember;
	}

	public void setMainMember(MainMember mainMember) {
		this.mainMember = mainMember;
	}

	public String getVipId() {
        return vipId;
    }

    public void setVipId(String vipId) {
        this.vipId = vipId;
    }

    public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public String getNotWelcome() {
		return notWelcome;
	}

	public void setNotWelcome(String notWelcome) {
		this.notWelcome = notWelcome;
	}

	public String getNewCaseNoIns() {
		return newCaseNoIns;
	}

	public void setNewCaseNoIns(String newCaseNoIns) {
		this.newCaseNoIns = newCaseNoIns;
	}

	public String getTrvlProdId() {
		return trvlProdId;
	}

	public void setTrvlProdId(String trvlProdId) {
		this.trvlProdId = trvlProdId;
	}

	/**
	 * == 整戶資料查詢中的主會員物件 ==
     *   "a190_bo":{
     *     "case_no":"A030748397",
     *     "vip_id":"H29376308F",
     *     "vip_name":"謝○玲",
     *     "vip_brdy":"1974-05-02",
     *     "mobile":"0922123456",
     *     "zip_code":"116",
     *     "addr":"興隆路二段 203 巷 52 號 1",
     *     "tel_area":"02",
     *     "tel":"0227551399",
     *     "email":"XXXXX@cathlife.com.tw",
     *     "aply_date":"2019-01-16",
     *     "card_no":"4284306866737233",
     *     "card_name_ym":"202206",
     *     "card_kind":"1",
     *     "agnt_id":"A17224802A",
     *     "agnt_name":"謝○玲",
     *     "agnt_mobile":"0922123456",
     *     "agnt_div_no":"A819213",
     *     "input_id":"T25969399A",
     *     "input_div_no":"9P06201",
     *     "input_name":"顏○庭",
     *     "vip_type":"1",
     *     "input_date":"2019-01-16 14:43:24.25",
     *     "aply_sts":"1",
     *     "agnt_licence":"0111100932",
     *     "case_no_ins":"E0810236241",
     *     "agnt_license_ins":"A15E583160",
     *     "national":"7",
     *     "national_memo":"中華民國",
     *     "risk_occu_code":"J800",
     *     "risk_jobtitle_code":"T010",
     *     "m_retMsg":{
     *       "m_desc":[
     *         ""
     *       ],
     *       "m_msgid":"",
     *       "m_sysid":"",
     *       "m_type":"",
     *       "m_url":"",
     *       "m_idx":0,
     *       "m_code":0
     *     }
     *   }
	 */
	@JsonIgnoreProperties(ignoreUnknown = true,
			value = {"CASE_NO", "zip_code", "addr", "tel_area", "tel",
					"tel_ext", "aply_date", "card_kind", "agnt_id", "agnt_name",
					"agnt_mobile", "agnt_div_no", "input_id", "input_div_no", "input_name",
					"vip_type", "input_date", "agnt_tel_area", "agnt_tel", "agnt_tel_ext",
					"aply_sts", "agnt_licence", "NATIONAL", "NATIONAL_MEMO", "RISK_OCCU_CODE",
					"RISK_JOBTITLE_CODE", "m_retMsg", "msgDesc", "type", "url",
					"sysid", "returnCode", "msgid", "RISK_JOBTITLE_CODE"})
	@JsonInclude(Include.NON_NULL)
	public static class MainMember {

		@JsonProperty("CASE_NO")
		private String caseNo;

		@JsonProperty("VIP_ID")
		private String vipId;

		@JsonProperty("VIP_NAME")
		private String vipName;

		@JsonProperty("VIP_BRDY")
		private String vipBirthday;

		@JsonProperty("MOBILE")
		private String mobile;

		@JsonProperty("zip_code")
		private String zipCode;

		@JsonProperty("addr")
		private String address;

		@JsonProperty("tel_area")
		private String telArea;

		@JsonProperty("tel")
		private String tel;

		@JsonProperty("tel_ext")
		private String telExt;

		@JsonProperty("EMAIL")
		private String email;

		@JsonProperty("aply_date")
		private String applyDate;

		@JsonProperty("CARD_NO")
		private String cardNo;

		@JsonProperty("CARD_NAME_YM")
		private String cardNameYm;

		@JsonProperty("card_kind")
		private String cardKind;

		@JsonProperty("agnt_id")
		private String agntId;

		@JsonProperty("agnt_name")
		private String agntName;

		@JsonProperty("agnt_mobile")
		private String agntMobile;

		@JsonProperty("agnt_div_no")
		private String agntDivNo;

		@JsonProperty("input_id")
		private String inputId;

		@JsonProperty("input_div_no")
		private String inputDivNo;

		@JsonProperty("input_name")
		private String inputName;

		@JsonProperty("vip_type")
		private String vipType;

		@JsonProperty("input_date")
		private String inputDate;

		@JsonProperty("agnt_tel_area")
		private String agntTelArea;

		@JsonProperty("agnt_tel")
		private String agntTel;

		@JsonProperty("agnt_tel_ext")
		private String agntTelExt;

		@JsonProperty("aply_sts")
		private String aplySts;

		@JsonProperty("agnt_licence")
		private String agntLicence;

		@JsonProperty("NATIONAL")
		private String national;

		@JsonProperty("CASE_NO_INS")
		private String caseNoIns;

		@JsonProperty("NATIONAL_MEMO")
		private String nationalMemo;

		@JsonProperty("RISK_OCCU_CODE")
		private String riskOccuCode;

		@JsonProperty("RISK_JOBTITLE_CODE")
		private String riskJobtitleCode;

		@JsonProperty("msgDesc")
        private String msgDesc;
		@JsonProperty("type")
        private String type;
		@JsonProperty("SEX")
        private String sex;
		@JsonProperty("sysid")
        private String sysid;
		@JsonProperty("msgid")
        private String msgid;
		@JsonProperty("url")
        private String url;
		@JsonProperty("returnCode")
        private String returnCode;

		@JsonProperty("m_retMsg")
        private CathayResponseReturnMessage mRetMsg;

		public String getCaseNo() {
			return caseNo;
		}

		public void setCaseNo(String caseNo) {
			this.caseNo = caseNo;
		}

		public String getVipId() {
			return vipId;
		}

		public void setVipId(String vipId) {
			this.vipId = vipId;
		}

		public String getVipName() {
			return vipName;
		}

		public void setVipName(String vipName) {
			this.vipName = vipName;
		}

		public String getVipBirthday() {
			return vipBirthday;
		}

		public void setVipBirthday(String vipBirthday) {
			this.vipBirthday = vipBirthday;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getZipCode() {
			return zipCode;
		}

		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getTelArea() {
			return telArea;
		}

		public void setTelArea(String telArea) {
			this.telArea = telArea;
		}

		public String getTel() {
			return tel;
		}

		public void setTel(String tel) {
			this.tel = tel;
		}

		public String getTelExt() {
			return telExt;
		}

		public void setTelExt(String telExt) {
			this.telExt = telExt;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getApplyDate() {
			return applyDate;
		}

		public void setApplyDate(String applyDate) {
			this.applyDate = applyDate;
		}

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}

		public String getCardNameYm() {
			return cardNameYm;
		}

		public void setCardNameYm(String cardNameYm) {
			this.cardNameYm = cardNameYm;
		}

		public String getCardKind() {
			return cardKind;
		}

		public CathayResponseReturnMessage getmRetMsg() {
			return mRetMsg;
		}

		public void setmRetMsg(CathayResponseReturnMessage mRetMsg) {
			this.mRetMsg = mRetMsg;
		}

		public void setCardKind(String cardKind) {
			this.cardKind = cardKind;
		}

		public String getAgntId() {
			return agntId;
		}

		public void setAgntId(String agntId) {
			this.agntId = agntId;
		}

		public String getAgntName() {
			return agntName;
		}

		public void setAgntName(String agntName) {
			this.agntName = agntName;
		}

		public String getAgntMobile() {
			return agntMobile;
		}

		public void setAgntMobile(String agntMobile) {
			this.agntMobile = agntMobile;
		}

		public String getAgntDivNo() {
			return agntDivNo;
		}

		public void setAgntDivNo(String agntDivNo) {
			this.agntDivNo = agntDivNo;
		}

		public String getInputId() {
			return inputId;
		}

		public void setInputId(String inputId) {
			this.inputId = inputId;
		}

		public String getInputDivNo() {
			return inputDivNo;
		}

		public void setInputDivNo(String inputDivNo) {
			this.inputDivNo = inputDivNo;
		}

		public String getInputName() {
			return inputName;
		}

		public void setInputName(String inputName) {
			this.inputName = inputName;
		}

		public String getVipType() {
			return vipType;
		}

		public void setVipType(String vipType) {
			this.vipType = vipType;
		}

		public String getInputDate() {
			return inputDate;
		}

		public void setInputDate(String inputDate) {
			this.inputDate = inputDate;
		}

		public String getAgntTelArea() {
			return agntTelArea;
		}

		public void setAgntTelArea(String agntTelArea) {
			this.agntTelArea = agntTelArea;
		}

		public String getAgntTel() {
			return agntTel;
		}

		public void setAgntTel(String agntTel) {
			this.agntTel = agntTel;
		}

		public String getAgntTelExt() {
			return agntTelExt;
		}

		public void setAgntTelExt(String agntTelExt) {
			this.agntTelExt = agntTelExt;
		}

		public String getAplySts() {
			return aplySts;
		}

		public void setAplySts(String aplySts) {
			this.aplySts = aplySts;
		}

		public String getAgntLicence() {
			return agntLicence;
		}

		public void setAgntLicence(String agntLicence) {
			this.agntLicence = agntLicence;
		}

		public String getNational() {
			return national;
		}

		public void setNational(String national) {
			this.national = national;
		}

		public String getCaseNoIns() {
			return caseNoIns;
		}

		public void setCaseNoIns(String caseNoIns) {
			this.caseNoIns = caseNoIns;
		}

		public String getNationalMemo() {
			return nationalMemo;
		}

		public void setNationalMemo(String nationalMemo) {
			this.nationalMemo = nationalMemo;
		}

		public String getRiskOccuCode() {
			return riskOccuCode;
		}

		public void setRiskOccuCode(String riskOccuCode) {
			this.riskOccuCode = riskOccuCode;
		}

		public String getRiskJobtitleCode() {
			return riskJobtitleCode;
		}

		public void setRiskJobtitleCode(String riskJobtitleCode) {
			this.riskJobtitleCode = riskJobtitleCode;
		}

        public String getMsgDesc() {
            return msgDesc;
        }

        public void setMsgDesc(String msgDesc) {
            this.msgDesc = msgDesc;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSex() {
        	String sexType = sex;
			
			switch(sex) {
				case "1":
					sexType = "先生";
					break;
				case "2":
					sexType = "小姐";
					break;
				default:
			}

			return sexType;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getSysid() {
            return sysid;
        }

        public void setSysid(String sysid) {
            this.sysid = sysid;
        }

        public String getMsgid() {
            return msgid;
        }

        public void setMsgid(String msgid) {
            this.msgid = msgid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
	}

	/**
	 * == 整戶資料查詢中的從屬會員物件 ==
	 *     {
	 *       "case_no":"A030748397",
	 *       "vip_id":"H29376308F",
	 *       "rela_id":"A13042355K",
	 *       "rela_name":"傅○熙",
	 *       "rela_brdy":"2008-05-28",
	 *       "mobile":"0922123456",
	 *       "rela_kind":"4",
	 *       "law_name":"",
	 *       "law_rela":"",
	 *       "rela_type":"1",
	 *       "national":"TW",
	 *       "case_no_ins":"E0810236241",
	 *       "national_memo":"中華民國",
	 *       "risk_occu_code":"J800",
	 *       "risk_jobtitle_code":"T010",
	 *       "m_retMsg":{
	 *         "m_desc":[
	 *           ""
	 *         ],
	 *         "m_msgid":"",
	 *         "m_sysid":"",
	 *         "m_type":"",
	 *         "m_url":"",
	 *         "m_idx":0,
	 *         "m_code":0
	 *       }
	 *     }
	 */
	@JsonIgnoreProperties(ignoreUnknown = true,
			value = {"CASE_NO", "mobile", "RELA_KIND", "LAW_NAME", "LAW_RELA",
					"RELA_TYPE", "NATIONAL", "NATIONAL_MEMO", "RISK_OCCU_CODE", "risk_jobtitle_code",
					"msgDesc", "type", "MOBILE", "sysid", "returnCode",
					"msgid", "url", "RISK_JOBTITLE_CODE"})
	@JsonInclude(Include.NON_NULL)
	public static class SubMember {

		@JsonProperty("CASE_NO")
		private String caseNo;

		@JsonProperty("VIP_ID")
		private String vipId;

		@JsonProperty("RELA_ID")
		private String relaId;

		@JsonProperty("RELA_NAME")
		private String relaName;

		@JsonProperty("RELA_BRDY")
		private String relaBrdy;

		@JsonProperty("MOBILE")
		private String mobile;

		@JsonProperty("RELA_KIND")
		private String relKind;

		@JsonProperty("LAW_NAME")
		private String lawName;

		@JsonProperty("LAW_RELA")
		private String laRela;

		@JsonProperty("RELA_TYPE")
		private String relaType;

		@JsonProperty("NATIONAL")
		private String national;

		@JsonProperty("CASE_NO_INS")
		private String caseNoIns;

		@JsonProperty("NATIONAL_MEMO")
		private String nationalMemo;

		@JsonProperty("RISK_OCCU_CODE")
		private String riskOccuCode;

		@JsonProperty("RISK_JOBTITLE_CODE")
		private String riskJobtitleCode;

		@JsonProperty("msgDesc")
        private String msgDesc;
		@JsonProperty("type")
        private String type;
		@JsonProperty("SEX")
        private String sex;
		@JsonProperty("sysid")
        private String sysid;
		@JsonProperty("msgid")
        private String msgid;
		@JsonProperty("url")
        private String url;
		@JsonProperty("returnCode")
        private String returnCode;
		
		public String getCaseNo() {
			return caseNo;
		}

		public void setCaseNo(String caseNo) {
			this.caseNo = caseNo;
		}

		public String getVipId() {
			return vipId;
		}

		public void setVipId(String vipId) {
			this.vipId = vipId;
		}

		public String getRelaId() {
			return relaId;
		}

		public void setRelaId(String relaId) {
			this.relaId = relaId;
		}

		public String getRelaName() {
			return relaName;
		}

		public void setRelaName(String relaName) {
			this.relaName = relaName;
		}

		public String getRelaBrdy() {
			return relaBrdy;
		}

		public void setRelaBrdy(String relaBrdy) {
			this.relaBrdy = relaBrdy;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getRelKind() {
			return relKind;
		}

		public void setRelKind(String relKind) {
			this.relKind = relKind;
		}

		public String getLawName() {
			return lawName;
		}

		public void setLawName(String lawName) {
			this.lawName = lawName;
		}

		public String getLaRela() {
			return laRela;
		}

		public void setLaRela(String laRela) {
			this.laRela = laRela;
		}

		public String getRelaType() {
			return relaType;
		}

		public void setRelaType(String relaType) {
			this.relaType = relaType;
		}

		public String getNational() {
			return national;
		}

		public void setNational(String national) {
			this.national = national;
		}

		public String getCaseNoIns() {
			return caseNoIns;
		}

		public void setCaseNoIns(String caseNoIns) {
			this.caseNoIns = caseNoIns;
		}

		public String getNationalMemo() {
			return nationalMemo;
		}

		public void setNationalMemo(String nationalMemo) {
			this.nationalMemo = nationalMemo;
		}

		public String getRiskOccuCode() {
			return riskOccuCode;
		}

		public void setRiskOccuCode(String riskOccuCode) {
			this.riskOccuCode = riskOccuCode;
		}



		public String getRiskJobtitleCode() {
			return riskJobtitleCode;
		}

		public void setRiskJobtitleCode(String riskJobtitleCode) {
			this.riskJobtitleCode = riskJobtitleCode;
		}

		public String getMsgDesc() {
			return msgDesc;
		}

		public void setMsgDesc(String msgDesc) {
			this.msgDesc = msgDesc;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSex() {
			String sexType = sex;
			
			switch(sex) {
				case "1":
					sexType = "先生";
					break;
				case "2":
					sexType = "小姐";
					break;
				default:
			}

			return sexType;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getSysid() {
			return sysid;
		}

		public void setSysid(String sysid) {
			this.sysid = sysid;
		}

		public String getMsgid() {
			return msgid;
		}

		public void setMsgid(String msgid) {
			this.msgid = msgid;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}
