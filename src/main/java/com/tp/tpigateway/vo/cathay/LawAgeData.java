package com.tp.tpigateway.vo.cathay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * == 取得會員法定年齡 ==
 * 國泰 易call保 API response JSON 的 data 物件
 *
 * "data": {
 *     "AGE": "50",
 *     "VIP_BRDY": "1970-01-0",
 *     "ISSUE_DATE": "2020-02-20"
 * }
 */
@JsonInclude(Include.NON_NULL)
public class LawAgeData {

	@JsonProperty("AGE")
	private String age;
	@JsonProperty("VIP_BRDY")
	private String vipBirthday;
	@JsonProperty("ISSUE_DATE")
	private String issueDate;

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getVipBirthday() {
		return vipBirthday;
	}

	public void setVipBirthday(String vipBirthday) {
		this.vipBirthday = vipBirthday;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

}