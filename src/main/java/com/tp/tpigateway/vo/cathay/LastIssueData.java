package com.tp.tpigateway.vo.cathay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * == 取得上一次投保紀錄  ==
 * 國泰 易call保 API response JSON 的 data 物件
 *
 * "data": {
 *     "BIRTHDAY": "0741222",
 *     "LOCAL_MEDICAL": "0",
 *     "AGE": "34",
 *     "IS_EMPLOYEE": "Y",
 *     "AGENT_QUALIFICATION": "Y",
 *     "OVERSEA_MEDICAL": "10",
 *     "OVERSEA_ILLNESS_TYPE": "3",
 *     "IS_MEMBER": "Y",
 *     "CREDITCARD_VALIDDATE": "202010",
 *     "LOCAL_MAIN": "100",
 *     "MEMBER_TYPE": "MAIN",
 *     "OVERSEA_ILLNESS": "20",
 *     "OVERSEA_MAIN": "100"
 * }
 */
public class LastIssueData {

	@JsonProperty("BIRTHDAY")
	private String birthday;
	
	@JsonProperty("LOCAL_MEDICAL")
	private String localMedical;
	
	@JsonProperty("AGE")
	private String age;
	
	@JsonProperty("IS_EMPLOYEE")
	private String isEmployee;
	
	@JsonProperty("AGENT_QUALIFICATION")
	private String agentQualification;
	
	@JsonProperty("OVERSEA_MEDICAL")
	private String overseaMedical;
	
	@JsonProperty("OVERSEA_ILLNESS_TYPE")
	private String overseaIllnessType;
	
	@JsonProperty("IS_MEMBER")
	private String isMember;
	
	@JsonProperty("CREDITCARD_VALIDDATE")
	private String creditcardValiddate;
	
	@JsonProperty("LOCAL_MAIN")
	private String localMain;
	
	@JsonProperty("MEMBER_TYPE")
	private String memberType;
	
	@JsonProperty("OVERSEA_ILLNESS")
	private String overseaIllness;
	
	@JsonProperty("OVERSEA_MAIN")
	private String overseaMain;

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getLocalMedical() {
		return localMedical;
	}

	public void setLocalMedical(String localMedical) {
		this.localMedical = localMedical;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getIsEmployee() {
		return isEmployee;
	}

	public void setIsEmployee(String isEmployee) {
		this.isEmployee = isEmployee;
	}

	public String getAgentQualification() {
		return agentQualification;
	}

	public void setAgentQualification(String agentQualification) {
		this.agentQualification = agentQualification;
	}

	public String getOverseaMedical() {
		return overseaMedical;
	}

	public void setOverseaMedical(String overseaMedical) {
		this.overseaMedical = overseaMedical;
	}

	public String getOverseaIllnessType() {
		return overseaIllnessType;
	}

	public void setOverseaIllnessType(String overseaIllnessType) {
		this.overseaIllnessType = overseaIllnessType;
	}

	public String getIsMember() {
		return isMember;
	}

	public void setIsMember(String isMember) {
		this.isMember = isMember;
	}

	public String getCreditcardValiddate() {
		return creditcardValiddate;
	}

	public void setCreditcardValiddate(String creditcardValiddate) {
		this.creditcardValiddate = creditcardValiddate;
	}

	public String getLocalMain() {
		return localMain;
	}

	public void setLocalMain(String localMain) {
		this.localMain = localMain;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getOverseaIllness() {
		return overseaIllness;
	}

	public void setOverseaIllness(String overseaIllness) {
		this.overseaIllness = overseaIllness;
	}

	public String getOverseaMain() {
		return overseaMain;
	}

	public void setOverseaMain(String overseaMain) {
		this.overseaMain = overseaMain;
	}
	
}
