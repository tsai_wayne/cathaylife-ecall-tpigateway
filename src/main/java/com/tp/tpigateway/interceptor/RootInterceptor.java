package com.tp.tpigateway.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.tp.tpigateway.util.HttpContextUtils;

public class RootInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
	        Object handler) throws Exception {
		String sessionId = HttpContextUtils.getSessionId();
		MDC.put("sessionId", sessionId);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
	        Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
	        Object handler, Exception ex)throws Exception {
		MDC.clear();
	}
}