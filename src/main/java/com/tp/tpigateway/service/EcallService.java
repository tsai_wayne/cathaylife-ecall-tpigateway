package com.tp.tpigateway.service;

import com.tp.tpigateway.vo.flow.FlowRequest;
import com.tp.tpigateway.vo.gateway.ContentAllVipData;
import com.tp.tpigateway.vo.gateway.ContentCheckAmtVoice;
import com.tp.tpigateway.vo.gateway.ContentConfirmVoice;
import com.tp.tpigateway.vo.gateway.ContentGetRgnCode;
import com.tp.tpigateway.vo.gateway.ContentLastIssueData;
import com.tp.tpigateway.vo.gateway.ContentLawAge;
import com.tp.tpigateway.vo.gateway.ContentTempVoice;
import com.tp.tpigateway.vo.gateway.GatewayResponse;

public interface EcallService {

	public GatewayResponse<ContentLawAge> lawAge(FlowRequest req);
	public GatewayResponse<ContentLastIssueData> lastIssueData(FlowRequest req);
	public GatewayResponse<ContentAllVipData> allVipData(FlowRequest req);
	public GatewayResponse<ContentConfirmVoice> doConfirmVoice(FlowRequest req);
	public GatewayResponse<ContentTempVoice> insertTempVoice(FlowRequest req);
	public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(FlowRequest req);
	public GatewayResponse<ContentGetRgnCode> getRgnCode(FlowRequest req);

}
