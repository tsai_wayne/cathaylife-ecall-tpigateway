package com.tp.tpigateway.service.impl;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tp.tpigateway.exception.TPException;
import com.tp.tpigateway.service.EcallService;
import com.tp.tpigateway.util.FeignCheckUtils;
import com.tp.tpigateway.util.JsonUtil;
import com.tp.tpigateway.vo.cathay.AllVipData;
import com.tp.tpigateway.vo.cathay.CathayBaseResponse;
import com.tp.tpigateway.vo.cathay.CathayResponseDetail;
import com.tp.tpigateway.vo.cathay.CheckAmtVoiceData;
import com.tp.tpigateway.vo.cathay.ConfirmVoiceData;
import com.tp.tpigateway.vo.cathay.LastIssueData;
import com.tp.tpigateway.vo.cathay.LawAgeData;
import com.tp.tpigateway.vo.cathay.TempVoiceData;
import com.tp.tpigateway.vo.constant.EcallEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.AllVipDataCodeMappingEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.CheckAmtVoiceCodeMappingEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.ConfirmVoiceCodeMappingEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.LastIssueDataCodeMappingEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.LawAgeCodeMappingEnum;
import com.tp.tpigateway.vo.constant.EcallEnum.TempVoiceCodeMappingEnum;
import com.tp.tpigateway.vo.flow.FlowRequest;
import com.tp.tpigateway.vo.flow.FlowRequest.InsdList;
import com.tp.tpigateway.vo.flow.FlowRequest.Places;
import com.tp.tpigateway.vo.gateway.ContentAllVipData;
import com.tp.tpigateway.vo.gateway.ContentCheckAmtVoice;
import com.tp.tpigateway.vo.gateway.ContentConfirmVoice;
import com.tp.tpigateway.vo.gateway.ContentGetRgnCode;
import com.tp.tpigateway.vo.gateway.ContentLastIssueData;
import com.tp.tpigateway.vo.gateway.ContentLawAge;
import com.tp.tpigateway.vo.gateway.ContentTempVoice;
import com.tp.tpigateway.vo.gateway.GatewayResponse;

/**
 * TODO: 所有 API 由 "國壽code" 轉 "問問code"，取得 "問問code" 的 enum 為 null (國壽回傳 API 文件之外的 code)，的處理方式
 */
@Service
public class EcallServiceImpl implements EcallService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EcallServiceImpl.class);

    private static final String CATHAY_SYSTEM_ERROR = "099";
    private static final String API_RESULT_SUCCESS = "000";

    @Value("${ecall.api.url.prefix}")
    private String ECALL_API_URL_PREFIX;

    @Value("${ecall.api.x-ibm-client-id}")
    private String ECALL_API_XIBM_CLIENT_ID;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * getRgnCode() 這個 method 需要用到 "國泰人壽 TPIGateway" 內的 CBA10001DataService
     */
//  @Autowired
//  private CBA10001DataService cba10001DataService;

    /**
     * == 取得會員法定年齡 (http://${host}:${port}/${context}/ecall/law-age) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "issueDate" : "2020-02-20",
     *       "vipBrdy" : ["1970-01-01", "1980-08-08"]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/lawAge
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {"ISSUE_DATE":"2020-02-20", "VIP_BRDY":"1970-01-01"}
     *
     *   response ->
     *   {
     *       "returnCode": "0",
     *       "detail": {
     *           "resultMsg": "",
     *           "shortMsg": "查詢成功",
     *           "resultCode": "000",
     *           "data": {
     *               "AGE":"50",
     *               "VIP_BRDY":"1970-01-01",
     *               "ISSUE_DATE":"2020-02-20"
     *           },
     *           "returnMessage": {"m_desc":[""], "m_msgid":"", "m_sysid":"", "m_type":"", "m_url":"", "m_idx":0, "m_code":0}
     *       },
     *       "returnMsg": "API success",
     *       "UUID": "CB_API1331_20200221_DEC228F4C2_T01",
     *       "hostname": "cxlcsxat01"
     *   }
     *
     *   ps. 任一筆查詢不成功，直接回傳結果至 Flow
     *
     *
     * 3. response to Chat Flow
     *   把國壽回傳 JSON 的 resultCode 轉為 問問代碼並設定，回傳至 flow JSON 的 RTN_CODE
     *
     *   {
     *       "msg": "success",
     *       "code": 0,
     *       "data": {
     *           "from": "bot",
     *           "showtime": "2020-03-05 11:40:20",
     *           "content": {
     *               "RTN_CODE": "000",
     *               "AGE": [12, 50],
     *               "ISSUE_DATE": "2020-02-20",
     *               "VIP_BRDY": ["1970-01-01", "1980-08-08"]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     *
     *   問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     *   -----------------------------------------------------------------------
     *   000       000          投保成功
     *   131       097          傳入參數有誤
     *   132       099          執行錯誤
     *
     * @author Tony Liu
     * @author Wayne Tsai
     */
    @Override
    public GatewayResponse<ContentLawAge> lawAge(FlowRequest req) {
        /* 2. */
        for (String vipBrdy : req.getVipBrdy()) {
            if (StringUtils.isBlank(vipBrdy)) {
                GatewayResponse<ContentLawAge> response = GatewayResponse.buildFail(
                        ContentLawAge.class, "parameter: vipBrdy is required!");
                return response;
            }
        }

        String apiUrl = ECALL_API_URL_PREFIX + "/lawAge";
        HttpHeaders apiHeaders = buildApiHttpHeader();

        List<String> vipBrdys = req.getVipBrdy();
        List<String> ages = new ArrayList<>(vipBrdys.size());
        String resultCode = null;
        String resultMsg = null;
        for (String vipBrdy : vipBrdys) {
            Map<String, String> apiParams = new HashMap<>();
            apiParams.put("ISSUE_DATE", req.getIssueDate());
            apiParams.put("VIP_BRDY", vipBrdy);
            HttpEntity<Map<String, String>> request = new HttpEntity<>(apiParams, apiHeaders);

            CathayBaseResponse<LawAgeData> apiResponse = null;
            try {
                ResponseEntity<CathayBaseResponse<LawAgeData>> responseEntity = restTemplate.exchange(apiUrl,
                        HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<LawAgeData>>() {});
                apiResponse = responseEntity.getBody();
            } catch (Exception ex) {
                LOGGER.error("Invoke cathay e-call API lawAge fail", ex);
                GatewayResponse<ContentLawAge> failResult = getFailResult(
                        ContentLawAge.class, LawAgeCodeMappingEnum.SYSTEM_ERROR.getAskaskCode());
                ContentLawAge content = failResult.getData().getContent();
                content.setIssueDate(req.getIssueDate());
                content.setVipBirthday(vipBrdys);
                return failResult;
            }
            LOGGER.info("Response from lawAge:\n" + JsonUtil.objectToJson(apiResponse));

            CathayResponseDetail<LawAgeData> detail = apiResponse.getDetail();
            resultCode = detail.getResultCode();
            resultMsg = detail.getResultMsg();

            if (!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
                GatewayResponse<ContentLawAge> failResult = getFailResult(ContentLawAge.class,
                        LawAgeCodeMappingEnum.fromCathayCode(resultCode).getAskaskCode());
                ContentLawAge content = failResult.getData().getContent();
                content.setIssueDate(req.getIssueDate());
                content.setVipBirthday(vipBrdys);
                return failResult;
            }
            ages.add(detail.getData().getAge());
        }

        String rtnCode = LawAgeCodeMappingEnum.fromCathayCode(resultCode).getAskaskCode();

        /* 3. */
        ContentLawAge resContent = new ContentLawAge(rtnCode, ages, req.getIssueDate(), vipBrdys);
        GatewayResponse.Data<ContentLawAge> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentLawAge> response = GatewayResponse.buildSuccess(ContentLawAge.class, resultMsg);
        response.setData(resData);
        return response;
    }

    /**
     * == 取得上一次投保紀錄  (http://${host}:${port}/${context}/ecall/last-issue-data)==
     *
     *1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A29091346D"
     *   }
     *
     *2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/last_issue_data
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {" VIP_ID ":"A123456789"}
     *
     *   response ->
     *   {
     *      "returnCode":"0",
     *      "detail":{
     *          "resultMsg":"",
     *          "shortMsg":"查詢成功",
     *          "resultCode":"000",
     *          "data":{
     *              "BIRTHDAY":"0741222",
     *              "LOCAL_MEDICAL":"0",
     *              "AGE":"34",
     *              "IS_EMPLOYEE":"Y",
     *              "AGENT_QUALIFICATION":"Y",
     *              "OVERSEA_MEDICAL":"10",
     *              "OVERSEA_ILLNESS_TYPE":"3",
     *              "IS_MEMBER":"Y",
     *              "CREDITCARD_VALIDDATE":"202010",
     *              "LOCAL_MAIN":"100",
     *              "MEMBER_TYPE":"MAIN",
     *              "OVERSEA_ILLNESS":"20",
     *              "OVERSEA_MAIN":"100"
     *          },
     *              "returnMessage":{
     *                  "m_desc":[
     *                  ""
     *                  ],
     *                  "m_msgid":"",
     *                  "m_sysid":"",
     *                  "m_type":"",
     *                  "m_url":"",
     *                  "m_idx":0,
     *                  "m_code":0
     *              }
     *      },
     *      "returnMsg":"API success",
     *      "UUID":"CB_API1328_20200221_866D181AD1_T01",
     *      "hostname":"cxlcsxat01"
     *   }
     *3. response to Chat Flow
     *  3.1 成功範例
     *      {
     *          "msg": "success",
     *          "code": 0,
     *          "data": {
     *              "from": "bot",
     *              "showtime": "2020-03-05 11:40:20",
     *              "content": {
     *                  "RTN_CODE": "000",
     *                  "LOCAL_MAIN": "100",
     *                  "LOCAL_MEDICAL" : "0",
     *                  "OVERSEA_MAIN" : "100",
     *                  "OVERSEA_MEDICAL": "10",
     *                  "OVERSEA_ILLNESS": "20",
     *                  "OVERSEA_ILLNESS_TYPE": "3"
     *              },
     *              "channel": "cathaylife",
     *              "source": "cathaylife",
     *              "role": "cathaylife",
     *              "fb": false,
     *              "fbIntent": null,
     *              "sbIntent": null
     *
     *          }
     *      }
     *
     *   3.2 失敗範例 (note: 一筆錯誤就視為系統錯誤)
     *      {
     *          "msg": "error",
     *          "code": -1,
     *          "data": {
     *              "from": "bot",
     *              "showtime": "2020-03-05 11:40:20",
     *              "content": {
     *                  "RTN_CODE": "122",
     *                  "LOCAL_MAIN": "",
     *                  "LOCAL_MEDICAL" : "",
     *                  "OVERSEA_MAIN" : "",
     *                  "OVERSEA_MEDICAL": "",
     *                  "OVERSEA_ILLNESS": "",
     *                  "OVERSEA_ILLNESS_TYPE": ""
     *              },
     *              "channel": "cathaylife",
     *              "source": "cathaylife",
     *              "role": "cathaylife",
     *              "fb": false,
     *              "fbIntent": null,
     *              "sbIntent": null
     *
     *          }
     *      }
     *4. 問問代碼與國泰代碼對照表
     *  問問代碼        國泰代碼        說明
     *  --------------------------------------------------
     *  000         000         查詢成功
     *  121         097         未通過輸入檢核
     *  098         098         查無最近一次投保資料
     *  122         099         系統有誤
     */
    @Override
    public GatewayResponse<ContentLastIssueData> lastIssueData(FlowRequest req) {
        FeignCheckUtils.checkIssueRequestRequired(req);

        String url = ECALL_API_URL_PREFIX + "/last_issue_data";
        HttpHeaders headers = buildApiHttpHeader();

        Map<String, Object> params = new HashMap<>();
        params.put("VIP_ID", req.getVipId());
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(params, headers);

        CathayBaseResponse<LastIssueData> response = null;
        try {
            ResponseEntity<CathayBaseResponse<LastIssueData>> responseEntity = restTemplate.exchange(url,
                    HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<LastIssueData>>() {});
            response = responseEntity.getBody();
        } catch (Exception ex) {
            LOGGER.error("Calling Ecall api lastIssueData occurs error:\n");
            return getFailResult(ContentLastIssueData.class,
                    LastIssueDataCodeMappingEnum.fromCathayCode(CATHAY_SYSTEM_ERROR).getAskaskCode());
        }
        LOGGER.info("Response from lastIssueData:\n" + JsonUtil.objectToJson(response));

        CathayResponseDetail<LastIssueData> detail = response.getDetail();
        String flowReturnCode =
                LastIssueDataCodeMappingEnum.fromCathayCode(detail.getResultCode()).getAskaskCode();

        if(!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
            return getFailResult(ContentLastIssueData.class, flowReturnCode);
        }

        LastIssueData lastIssueData = detail.getData();

        ContentLastIssueData contentLastIssueData =
                new ContentLastIssueData(
                        flowReturnCode,
                        lastIssueData.getLocalMain(),
                        lastIssueData.getLocalMedical(),
                        lastIssueData.getOverseaMain(),
                        lastIssueData.getOverseaMedical(),
                        lastIssueData.getOverseaIllness(),
                        lastIssueData.getOverseaIllnessType());

        GatewayResponse<ContentLastIssueData> res =
                GatewayResponse.buildSuccess(ContentLastIssueData.class, response.getReturnMsg());
        GatewayResponse.Data<ContentLastIssueData> resData = GatewayResponse.buildDefaultData();
        resData.setContent(contentLastIssueData);
        res.setData(resData);
        return res;
    }

    /**
     * == 整戶資料查詢  (http://${host}:${port}/${context}/ecall/all-vip-data)==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A29091346D"
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/all_vip_data
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {" VIP_ID ":"A123456789"}
     *
     *   2.1 回傳代碼為097, 098時, data欄位為字串
     *   response ->
     *   {
     *      "returnCode":"0",
     *      "detail":{
     *          "resultMsg":"傳入參數:會員 ID 格式有誤",
     *          "shortMsg":"未通過輸入檢核",
     *          "resultCode":"097",
     *          "data":"A12345",
     *          "returnMessage":{
     *               "m_desc":[
     *              ""
     *              ],
     *              "m_msgid":"",
     *              "m_sysid":"",
     *              "m_type":"",
     *              "m_url":"",
     *              "m_idx":0,
     *              "m_code":-3
     *          }
     *      },
     *      "returnMsg":"API success",
     *      "UUID":"CB_API0467_20200130_74C2D87F54_S02",
     *      "hostname":"cxl1csas01"
     *   }
     *   2.2 查詢成功
     *   response -> 參考國泰api文件
     *
     * 3. response to Chat Flow
     *  3.1 成功範例
     *  {
     *    "msg": "success",
     *    "code": 0,
     *    "data": {
     *         "from": "bot",
     *         "showtime": "2020-03-05 11:40:20",
     *         "content": {
     *             "RTN_CODE": "000",
     *             "checkAGNT_ID": "00",
     *             "PROD_ID": "Y",
     *             "TRVL_PROD_ID": "M",
     *             "onlyVIP": "N",
     *             "newCASE_NO": "Y",
     *             "newCASE_NO_INS": "Y",
     *             "notWelcome": "00",
     *             "a190_bo": {
     *                 "vip_id": "H29376308F",
     *                 "vip_name": "謝曉玲",
     *                 "vip_brdy": "1955-01-01",
     *                 "mobile": "0922123456",
     *                 "email": "XXXXX@cathlife.com.tw",
     *                 "card_no": "4284306866737233",
     *                 "card_name_ym": "202206",
     *                 "case_no_ins": "E0810236241"
     *             },
     *             "a191_boList": [
     *                 {
     *                     "vip_id": "H29376308F",
     *                     "rela_id": "A13042355K",
     *                     "rela_name": "傅承熙",
     *                     "rela_brdy": "1957-01-01",
     *                     "case_no_ins": "E0810236241"
     *                 },
     *                 {
     *                     "vip_id": "H29376308F",
     *                     "rela_id": "A71912213K",
     *                     "rela_name": "傅小明",
     *                     "rela_brdy": "1990-01-01",
     *                     "case_no_ins": "E0810236241"
     *                  }
     *               ]
     *          },
     *      "channel": "cathaylife",
     *      "source": "cathaylife",
     *      "role": "cathaylife",
     *      "fb": false,
     *      "fbIntent": null,
     *      "sbIntent": null
     *   }
     * }
     *   3.2 失敗範例
     *  {
     *     "msg": "error",
     *     "code": -1,
     *     "data": {
     *          "from": "bot",
     *          "showtime": "2020-03-05 11:40:20",
     *          "content": {
     *              "RTN_CODE": "098",
     *              "checkAGNT_ID": "",
     *              "PROD_ID": "",
     *              "TRVL_PROD_ID": "",
     *              "onlyVIP": "",
     *              "newCASE_NO": "",
     *              "newCASE_NO_INS": "",
     *              "notWelcome": "",
     *              "a190_bo": {
     *                  "vip_id": "",
     *                  "vip_name": "",
     *                  "vip_brdy": "",
     *                  "mobile": "",
     *                  "email": "",
     *                  "card_no": "",
     *                  "card_name_ym": "",
     *                  "case_no_ins": ""
     *               },
     *               "a191_boList": []
     *           },
     *           "channel": "cathaylife",
     *           "source": "cathaylife",
     *           "role": "cathaylife",
     *           "fb": false,
     *           "fbIntent": null,
     *           "sbIntent": null
     *      }
     *  }
     *4. 問問代碼與國泰代碼對照表
     *  問問代碼        國泰代碼        說明
     *  --------------------------------------------------
     *  000         000         查詢成功
     *  001         001         檢核經手人有誤
     *  002         002         壽險/產險特殊名單
     *  096         096         舊件會員
     *  101         097         傳入參數有誤
     *  098         098         查無會員資料
     *  102         099         系統有誤
     */
    @Override
    public GatewayResponse<ContentAllVipData> allVipData(FlowRequest req) {
        FeignCheckUtils.checkIssueRequestRequired(req);

        String url = ECALL_API_URL_PREFIX + "/all_vip_data";
        HttpHeaders headers = buildApiHttpHeader();

        Map<String, Object> params = new HashMap<>();
        params.put("VIP_ID", req.getVipId());
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(params, headers);

        CathayBaseResponse<?> response = null;
        try {
            ResponseEntity<CathayBaseResponse<?>> responseEntity = restTemplate.exchange(url,
                    HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<?>>() {});
            response = responseEntity.getBody();
        } catch (Exception ex) {
            LOGGER.error("Calling Ecall api lastIssueData occurs error:\n");
            return getFailResult(ContentAllVipData.class,
                    AllVipDataCodeMappingEnum.fromCathayCode(CATHAY_SYSTEM_ERROR).getAskaskCode());
        }
        LOGGER.info("Response from allVipData:\n" + JsonUtil.objectToJson(response));

        CathayResponseDetail<?> detail = response.getDetail();
        String flowReturnCode =
                AllVipDataCodeMappingEnum.fromCathayCode(detail.getResultCode()).getAskaskCode();

        if(!API_RESULT_SUCCESS.equals(detail.getResultCode())) {
            return getFailResult(ContentAllVipData.class, flowReturnCode);
        }

        @SuppressWarnings("unchecked")
		AllVipData allVipData = JsonUtil.mapToObject((Map<String, Object>)detail.getData(), AllVipData.class);

        ContentAllVipData contentAllVipData = new ContentAllVipData();
        contentAllVipData.setReturnCode(flowReturnCode);
        contentAllVipData.setCheckAgntId(allVipData.getCheckAgntId());
        contentAllVipData.setProdId(allVipData.getProdId());
        contentAllVipData.setTrvlProdId(allVipData.getTrvlProdId());
        contentAllVipData.setOnlyVip(allVipData.getOnlyVip());
        contentAllVipData.setNewCaseNo(allVipData.getNewCaseNo());
        contentAllVipData.setNewCaseNoIns(allVipData.getNewCaseNoIns());
        contentAllVipData.setNotWelcome(allVipData.getNotWelcome());
        contentAllVipData.setMainMember(allVipData.getMainMember());
        contentAllVipData.setSubMemberList(allVipData.getSubMemberList());

        GatewayResponse<ContentAllVipData> res =
                GatewayResponse.buildSuccess(ContentAllVipData.class, response.getReturnMsg());
        GatewayResponse.Data<ContentAllVipData> resData = GatewayResponse.buildDefaultData();
        resData.setContent(contentAllVipData);
        res.setData(resData);
        return res;
    }

    /**
     * == 契約成立 (http://${host}:${port}/${context}/ecall/do-confirm-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "APLY_NO": "0010220330"
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/doConfirm_voice
     *   認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter -> {"APLY_NO": "0010700001"}
     *
     *   response ->
     *   {
     *       "returnCode":"0",
     *       "detail":{
     *           "resultMsg": "",
     *           "shortMsg": "契約成立",
     *           "resultCode": "000",
     *           "data": {
     *               "APLY_NO": "0010700001"
     *           },
     *           "returnMessage": {"m_desc":[""], "m_msgid":"", "m_sysid":"", "m_type":"", "m_url":"", "m_idx":0, "m_code":0}
     *       },
     *       "returnMsg": "API success",
     *       "UUID": "CB_API1330_20200318_620B18880A_T01",
     *       "hostname": "cxlcsxat01"
     *   }
     *
     *   必填參數: VIP_ID, INSD_ID, INSD_BRDY
     *
     *
     * 3. response to Chat Flow
     *   把國壽回傳 JSON 的 resultCode 轉為 問問代碼並設定，回傳至 flow JSON 的 RTN_CODE
     *   {
     *       "msg": "success",
     *       "code": 0,
     *       "data": {
     *           "from": "bot",
     *           "showtime": "2020-03-05 11:40:20",
     *           "content": {
     *               "RTN_CODE":"000",
     *               "APLY_NO":"0010220330"
     *           },
     *           "channel": "cathaylife",
     *           "source": "cathaylife",
     *           "role": "cathaylife",
     *           "fb": false,
     *           "fbIntent": null,
     *           "sbIntent": null
     *       }
     *   }
     *
     *   問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     *   -----------------------------------------------------------------------
     *   000      000         投保成功
     *   151      097         傳入參數有誤
     *   152      098         查無核保試算資料
     *   153      099         執行錯誤
     *
     * @author Tony Liu
     */
    @Override
    public GatewayResponse<ContentConfirmVoice> doConfirmVoice(FlowRequest req) {
        /* 2. */
        if (StringUtils.isBlank(req.getAplyNo())) {
            GatewayResponse<ContentConfirmVoice> response = GatewayResponse.buildFail(
                    ContentConfirmVoice.class, "parameter: aplyNo is required!");
            return response;
        }

        String apiUrl = ECALL_API_URL_PREFIX + "/doConfirm_voice";
        HttpHeaders apiHeaders = buildApiHttpHeader();

        Map<String, String> apiParams = new HashMap<>();
        apiParams.put("APLY_NO", req.getAplyNo());
        HttpEntity<Map<String, String>> request = new HttpEntity<>(apiParams, apiHeaders);

        CathayBaseResponse<ConfirmVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<ConfirmVoiceData>> responseEntity = restTemplate.exchange(apiUrl,
                    HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<ConfirmVoiceData>>() {});
            apiResponse = responseEntity.getBody();
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API insertTemp_voice fail", ex);
            return getFailResult(ContentConfirmVoice.class, ConfirmVoiceCodeMappingEnum.SYSTEM_ERROR.getAskaskCode());
        }
        CathayResponseDetail<ConfirmVoiceData> apiResponseDetail = apiResponse.getDetail();
        String rtnCode = ConfirmVoiceCodeMappingEnum.fromCathayCode(apiResponseDetail.getResultCode()).getAskaskCode();

        /* 3. */
        ContentConfirmVoice resContent = new ContentConfirmVoice(rtnCode, apiResponseDetail.getData());
        GatewayResponse.Data<ContentConfirmVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentConfirmVoice> response = GatewayResponse.buildSuccess(
                ContentConfirmVoice.class, apiResponseDetail.getShortMsg());
        response.setData(resData);
        return response;
    }

    /**
     * == 暫存 (http://${host}:${port}/${context}/ecall/insert-temp-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A123456789",
     *       "aplyNo": "",
     *       "issueDate": "2020-03-20",
     *       "bgnTime":"08:00:00",
     *       "issueDays":"3",
     *       "rgnCode":"3",
     *       "insdList":[
     *           {
     *               "insdId": "A123456700",
     *               "insdBrdy": "1987-01-25",
     *               "hdAmt": "1000",
     *               "hpAmt": "100",
     *               "hkType": "2",
     *               "hkAmt": "10",
     *               "omtpAmt": "1",
     *               "trvlNotConv": "C"
     *           }
     *       ]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathayholdings-uat/cxl-voice-travel-insr-api/v1/insertTemp_voice
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter ->
     *     {
     *         "VIP_ID":"N26648942K",
     *         "APLY_NO":"0010700001",
     *         "ISSUE_DATE":"2020-03-20",
     *         "BGN_TIME":"08:00:00",
     *         "ISSUE_DAYS":"3",
     *         "RGN_CODE":"10",
     *         "SRC":"G",
     *         "INSD_LIST":[
     *             {
     *                 "INSD_ID":"N26648942K",
     *                 "INSD_BRDY":"1990-07-29",
     *                 "HD_AMT":"100",
     *                 "HP_AMT":"10",
     *                 "HK_TYPE":"3",
     *                 "HK_AMT":"10",
     *                 "OMTP_AMT":"1",
     *                 "TRVL_NOT_CONV":"N"
     *             },
     *             ...
     *         ]
     *     }
     *
     *     必填參數: VIP_ID, INSD_ID, INSD_BRDY
     *
     *
     * 3. response to Chat Flow
     *   把國壽回傳 JSON 的 resultCode 轉為 問問代碼並設定，回傳至 flow JSON 的 RTN_CODE
     *
     *   {
     *       "msg":"success",
     *       "code":0,
     *       "data":{
     *           "from":"bot",
     *           "showtime":"2020-03-05 11:40:20",
     *           "content":{
     *               "RTN_CODE":"000",
     *               "APLY_NO":"0010220330",
     *               "ISSUE_DATE":"2020-03-20",
     *               "BGN_TIME":"08:00:00",
     *               "ISSUE_DAYS":"3",
     *               "RGN_CODE":"3",
     *               "INSD_LIST": [
     *                   {
     *                       "INSD_ID":"A123456789",
     *                       "INSD_BRDY":"1990-07-29",
     *                       "HD_AMT":"1000",
     *                       "HP_AMT":"100",
     *                       "HK_TYPE":"1",
     *                       "HK_AMT":"10",
     *                       "OMTP_AMT":"2",
     *                       "TRVL_NOT_CONV":"C"
     *                   }
     *               ]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     *
     *   問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     *   -----------------------------------------------------------------------
     *   000       000          暫存成功
     *   110       097          未通過輸入檢核
     *   111       099          系統有誤
     */
    @Override
    public GatewayResponse<ContentTempVoice> insertTempVoice(FlowRequest req) {
        /* 2. */
        Set<String> invalidFields = new HashSet<>();
        if (StringUtils.isBlank(req.getVipId())) {
            invalidFields.add("vipId");
        }
        if (req.getInsdList() == null) {
            invalidFields.add("insdList");
        } else {
            for (InsdList insd : req.getInsdList()) {
                if (StringUtils.isBlank(insd.getInsdId())) {
                    invalidFields.add("insdId");
                }
                if (StringUtils.isBlank(insd.getInsdBrdy())) {
                    invalidFields.add("insdBrdy");
                }
            }
        }
        if (!invalidFields.isEmpty()) {
            GatewayResponse<ContentTempVoice> response = GatewayResponse.buildFail(
                    ContentTempVoice.class, "parameter: " + invalidFields + " is required!");
            return response;
        }

        String apiUrl = ECALL_API_URL_PREFIX + "/insertTemp_voice";
        HttpHeaders apiHeaders = buildApiHttpHeader();
        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("VIP_ID", req.getVipId());
        apiParams.put("APLY_NO", req.getAplyNo());
        apiParams.put("ISSUE_DATE", req.getIssueDate());
        apiParams.put("BGN_TIME", req.getBgnTime());
        apiParams.put("ISSUE_DAYS", req.getIssueDays());
        apiParams.put("RGN_CODE", req.getRgnCode());
        apiParams.put("SRC", "G"); // 固定傳 "G"
        List<Map<String, String>> insdList = new ArrayList<>();
        for (InsdList insd : req.getInsdList()) {
            Map<String, String> insdMap = new HashMap<>();
            insdMap.put("INSD_ID", insd.getInsdId());
            insdMap.put("INSD_BRDY", insd.getInsdBrdy());
            insdMap.put("HD_AMT", insd.getHdAmt());
            insdMap.put("HP_AMT", insd.getHpAmt());
            insdMap.put("HK_TYPE", insd.getHkType());
            insdMap.put("HK_AMT", insd.getHkAmt());
            insdMap.put("OMTP_AMT", insd.getOmtpAmt());
            insdMap.put("TRVL_NOT_CONV", insd.getTrvlNotConv());
        }
        apiParams.put("INSD_LIST", insdList);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);
        CathayBaseResponse<TempVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<TempVoiceData>> responseEntity = restTemplate.exchange(apiUrl,
                    HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<TempVoiceData>>() {});
            apiResponse = responseEntity.getBody();
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API insertTemp_voice fail", ex);
            GatewayResponse<ContentTempVoice> response = getFailResult(ContentTempVoice.class,
                    TempVoiceCodeMappingEnum.SYSTEM_ERROR.getAskaskCode());
            return response;
        }
        CathayResponseDetail<TempVoiceData> detail = apiResponse.getDetail();
        TempVoiceCodeMappingEnum askCode = EcallEnum.TempVoiceCodeMappingEnum.fromCathayCode(detail.getResultCode());

        /* 3. */
        ContentTempVoice resContent = new ContentTempVoice(askCode.getAskaskCode(), detail.getData());
        GatewayResponse.Data<ContentTempVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentTempVoice> response = GatewayResponse.buildSuccess(ContentTempVoice.class, detail.getResultMsg());
        response.setData(resData);
        return response;
    }

    /**
     * == 核保試算 (http://${host}:${port}/${context}/ecall/do-checkamt-voice) ==
     *
     * 1. Chat Flow request
     *   {
     *       "channel": "cathaylife",
     *       "source": "cathaylife",
     *       "role": "cathaylife",
     *       "fbIntent": "",
     *       "sbIntent": "",
     *       "vipId" : "A123456789",
     *       "aplyNo": "",
     *       "issueDate": "2020-02-20",
     *       "bgnTime":"08:00:00",
     *       "issueDays":"3",
     *       "rgnCode":"3",
     *       "insdList":[
     *           {
     *               "insdId": "A123456700",
     *               "insdBrdy": "1987-01-25",
     *               "hdAmt": "1000",
     *               "hpAmt": "100",
     *               "hkType": "2",
     *               "hkAmt": "10",
     *               "omtpAmt": "1",
     *               "trvlNotConv": "C"
     *           }
     *       ]
     *   }
     *
     * 2. cathay API
     *   API URL   -> https://sapi.cathayholdings.com/cathayholdings/cathdoCheckAmt_voiceayholdings-uat/cxl-voice-travel-insr-api/v1/
     *   _認證方式     -> 在 Http Headers 填上 x-ibm-client-id：與註冊 APP 後取得的 id
     *   parameter ->
     *     {
     *         "VIP_ID":"N26648942K",
     *         "APLY_NO":"0010700001",
     *         "ISSUE_DATE":"2020-03-20",
     *         "BGN_TIME":"08:00:00",
     *         "ISSUE_DAYS":"3",
     *         "RGN_CODE":"10",
     *         "SRC":"G",
     *         "INSD_LIST":[
     *             {
     *                 "INSD_ID":"N26648942K",
     *                 "INSD_BRDY":"1990-07-29",
     *                 "HD_AMT":"100",
     *                 "HP_AMT":"10",
     *                 "HK_TYPE":"3",
     *                 "HK_AMT":"10",
     *                 "OMTP_AMT":"1",
     *                 "TRVL_NOT_CONV":"N"
     *             },
     *             ...
     *         ]
     *     }
     *
     * 3. response to Chat Flow
     *   把國壽回傳 JSON 的 resultCode 轉為 問問代碼並設定，回傳至 flow JSON 的 RTN_CODE
     *
     *   {
     *       "msg":"success",
     *       "code":0,
     *       "data":{
     *           "from":"bot",
     *           "showtime":"2020-03-05 11:40:20",
     *           "content":{
     *               "RTN_CODE":"000",
     *               "RGN_CODE":"3",
     *               "ISSUE_DATE":"2020-02-20",
     *               "BGN_TIME":"08:00:00",
     *               "ISSUE_DAYS":"3",
     *               "APLY_NO":"0010700001",
     *               "TOT_PREM":"2000",
     *               "INSD_LIST":[
     *                   {
     *                       "INSD_ID":"A123456789",
     *                       "INSD_BRDY":"1990-07-29",
     *                       "HD_AMT":"1000",
     *                       "HP_AMT":"100",
     *                       "HK_TYPE":"1",
     *                       "HK_AMT":"10",
     *                       "OMTP_AMT":"2",
     *                       "TRVL_NOT_CONV":"C",
     *                       "SELF_PREM":"1000"
     *                   }
     *               ]
     *           },
     *           "channel":"cathaylife",
     *           "source":"cathaylife",
     *           "role":"cathaylife",
     *           "fb":false,
     *           "fbIntent":null,
     *           "sbIntent":null
     *       }
     *   }
     *
     *   問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     *   -----------------------------------------------------------------------
     *   000       000          核保試算成功
     *   141       011          未通過洗錢檢核
     *   012       012          內外部通算未通過
     *   142       097          未通過輸入檢核
     *   143       099          系統有誤
     */
    @Override
    public GatewayResponse<ContentCheckAmtVoice> doCheckAmtVoice(FlowRequest req) {
        /* 2. */
        String apiUrl = ECALL_API_URL_PREFIX + "/doCheckAmt_voice";
        HttpHeaders apiHeaders = buildApiHttpHeader();

        Map<String, Object> apiParams = new HashMap<>();
        apiParams.put("VIP_ID", req.getVipId());
        apiParams.put("APLY_NO", req.getAplyNo());
        apiParams.put("ISSUE_DATE", req.getIssueDate());
        apiParams.put("BGN_TIME", req.getBgnTime());
        apiParams.put("ISSUE_DAYS", req.getIssueDays());
        apiParams.put("RGN_CODE", req.getRgnCode());
        apiParams.put("SRC", "G"); // 固定傳 "G"
        List<Map<String, String>> insdList = new ArrayList<>();
        for (InsdList insd : req.getInsdList()) {
            Map<String, String> insdMap = new HashMap<>();
            insdMap.put("INSD_ID", insd.getInsdId());
            insdMap.put("INSD_BRDY", insd.getInsdBrdy());
            insdMap.put("HD_AMT", insd.getHdAmt());
            insdMap.put("HP_AMT", insd.getHpAmt());
            insdMap.put("HK_TYPE", insd.getHkType());
            insdMap.put("HK_AMT", insd.getHkAmt());
            insdMap.put("OMTP_AMT", insd.getOmtpAmt());
            insdMap.put("TRVL_NOT_CONV", insd.getTrvlNotConv());
            insdList.add(insdMap);
        }
        apiParams.put("INSD_LIST", insdList);

        HttpEntity<Map<String, Object>> request = new HttpEntity<>(apiParams, apiHeaders);
        CathayBaseResponse<CheckAmtVoiceData> apiResponse = null;
        try {
            ResponseEntity<CathayBaseResponse<CheckAmtVoiceData>> responseEntity = restTemplate.exchange(apiUrl,
                    HttpMethod.POST, request, new ParameterizedTypeReference<CathayBaseResponse<CheckAmtVoiceData>>() {});
            apiResponse = responseEntity.getBody();
        } catch (Exception ex) {
            LOGGER.error("Invoke cathay e-call API doCheckAmt_voice fail", ex);
            GatewayResponse<ContentCheckAmtVoice> response = getFailResult(ContentCheckAmtVoice.class,
                    CheckAmtVoiceCodeMappingEnum.SYSTEM_ERROR.getAskaskCode());
            return response;
        }
        CathayResponseDetail<CheckAmtVoiceData> detail = apiResponse.getDetail();
        CheckAmtVoiceCodeMappingEnum askCode = EcallEnum.CheckAmtVoiceCodeMappingEnum.fromCathayCode(detail.getResultCode());

        /* 3. */
        ContentCheckAmtVoice resContent = new ContentCheckAmtVoice(askCode.getAskaskCode(), detail.getData());
        GatewayResponse.Data<ContentCheckAmtVoice> resData = GatewayResponse.buildDefaultData();
        resData.setContent(resContent);
        GatewayResponse<ContentCheckAmtVoice> response = GatewayResponse.buildSuccess(ContentCheckAmtVoice.class, detail.getResultMsg());
        response.setData(resData);
        return response;
    }

    /**
     *   問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     *   -----------------------------------------------------------------------
     *   000      000         投保成功
     *   161      097         傳入參數有誤
     *   162      099         執行錯誤
     */
    @Override
    public GatewayResponse<ContentGetRgnCode> getRgnCode(FlowRequest req) {
        List<Places> places = req.getPlaces();

//      List<Map<String, Object>> placeMap = JsonUtil.objectToMap(places);
        @SuppressWarnings("unchecked")
        List<Object> placeMap = (List<Object>) JsonUtil.objectToMap(places);
        Map<String, Object> rgnCodeMap = null;
//        try {
//            rgnCodeMap = cba10001DataService.getRGN_CODE(req.getOrigStr(), placeMap);
//        } catch (Exception ex) {
//            LOGGER.error("Calling Cathay CBA10001DataService getRGN_CODE() fail!");
//            return getFailResult(ContentGetRgnCode.class, RgnCodeCodeMappingEnum.SYSTEM_ERROR.getAskaskCode());
//        }
        return null;
    }


    private HttpHeaders buildApiHttpHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("x-ibm-client-id", ECALL_API_XIBM_CLIENT_ID);
        return headers;
    }


    /*
     * _建立 fail response，參數 contentType 為 GatewayResponse 的 Data 的 Content 的泛形類別
     * _使用 reflection 建立 content 物件，並對 return code 設值
     *
     * *************************************************************************
     * _如果 contentType 的類別沒有宣告 returnCode 這個 field，則回傳的 JSON 在 content 內的 RTN_CODE 會是 null
     * _並在 JSON 的 msg 顯示， 類別aa.bb.Ccc呼叫setRreturnCode()失敗
     * *************************************************************************
     *
     * @param contentType    - 回傳 flow 的 JSON，content 對應的 Java 類別
     * @param flowReturnCode - 回傳 flow 的 JSON，content 物件內的 RTN_CODE 的值
     */
    private <T> GatewayResponse<T> getFailResult(Class<T> contentType, String flowReturnCode) {
        T content;
        try {
            content = contentType.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new TPException(("初始化" + contentType.getName() + "失敗"), 500, ex);
        }

        String errMsg = "error";
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor("returnCode", content.getClass());
            pd.getWriteMethod().invoke(content, flowReturnCode);
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//          return GatewayResponse.buildFail(contentType, "類別" + contentType.getName() + "呼叫setRreturnCode()失敗");
            errMsg = "類別 " + contentType.getName() + " 無法設定 RTN_CODE";
        }

        GatewayResponse<T> failResult = GatewayResponse.buildFail(contentType, errMsg);
        GatewayResponse.Data<T> failResData = GatewayResponse.buildDefaultData();
        failResData.setContent(content);
        failResult.setData(failResData);
        return failResult;
    }

}
