package com.tp.tpigateway.mock;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cathayholdings-uat/cxl-voice-travel-insr-api/v1")
public class FakeController {

    /**
     * {
     *   "ISSUE_DATE": "2020-02-20",
     *   "VIP_BRDY ": "1970-01-01"
     * }
     *
     *
     * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     * -----------------------------------------------------------------------
     * 000       000          投保成功
     * 131       097          傳入參數有誤
     * 132       099          執行錯誤
     */
	@RequestMapping(value = {"/lawAge"},
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String lawAge(@RequestBody Map<String, String> param) {
		String vipBrdy = param.get("VIP_BRDY");

		int returnCode = -1;
        String shortMsg = "";
        String resultCode = null;
        String resultMsg = null;
        String age = null;
        if (vipBrdy == null || vipBrdy.endsWith("97")) {
            resultCode = "097";
            resultMsg = "傳入參數:會員生日必須為西元日期";
            shortMsg = "未通過輸入檢核";
        } else if (vipBrdy.endsWith("99")) {
            resultCode = "099";
            resultMsg = "執行錯誤";
        } else {
            returnCode = 0;
            shortMsg = "查詢成功";
            resultCode = "000";
            resultMsg = "";
            age = "35";
        }

		return "{\r\n" +
		        "    \"returnCode\":\"" + returnCode + "\",\r\n" +
		        "    \"detail\":{\r\n" +
		        "        \"resultMsg\":\"" + resultMsg + "\",\r\n" +
		        "        \"shortMsg\":\"" + shortMsg + "\",\r\n" +
		        "        \"resultCode\":\"" + resultCode + "\",\r\n" +
		        "        \"data\":{\r\n" +
		        "            \"AGE\":\"" + age + "\",\r\n" +
		        "            \"VIP_BRDY\":\"1970-01-01\",\r\n" +
		        "            \"ISSUE_DATE\":\"2020-02-20\"\r\n" +
		        "        },\r\n" +
		        "        \"returnMessage\":{\r\n" +
		        "            \"m_desc\":[\r\n" +
		        "                \"\"\r\n" +
		        "            ],\r\n" +
		        "            \"m_msgid\":\"\",\r\n" +
		        "            \"m_sysid\":\"\",\r\n" +
		        "            \"m_type\":\"\",\r\n" +
		        "            \"m_url\":\"\",\r\n" +
		        "            \"m_idx\":0,\r\n" +
		        "            \"m_code\":0\r\n" +
		        "        }\r\n" +
		        "    },\r\n" +
		        "    \"returnMsg\":\"API success\",\r\n" +
		        "    \"UUID\":\"CB_API1331_20200221_DEC228F4C2_T01\",\r\n" +
		        "    \"hostname\":\"cxlcsxat01\"\r\n" +
		        "}";
	}

	/**
     * {"VIP_ID":"A123456789"}
     *
     * VIP_ID
     * _尾碼0 -> 傳入參數有誤
     * _尾碼1 -> 查無會員資料
     * _尾碼2 -> 系統有誤
     * _其他   -> 查詢正常
     *
     */
	@RequestMapping(value = "last_issue_data",
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String lastIssueData(@RequestBody Map<String, String> param) {
		String vipId = param.get("VIP_ID");
		String result = null;

		if(vipId.endsWith("0")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"傳入參數:會員 ID 格式有誤\",\r\n" +
					"    \"shortMsg\":\"未通過輸入檢核\",\r\n" +
					"    \"resultCode\":\"097\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"傳入參數:會員 ID 格式有誤\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-12\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API1328_20200221_588019BD21_T01\",\r\n" +
					"  \"hostname\":\"cxlcsxat01\"\r\n" +
					"}";
		} else if(vipId.endsWith("1")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"\",\r\n" +
					"    \"shortMsg\":\"查無最近一次投保資料\",\r\n" +
					"    \"resultCode\":\"098\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API1328_20200221_92FCAF3195_T01\",\r\n" +
					"  \"hostname\":\"cxlcsxat01\"\r\n" +
					"}";
		} else if(vipId.endsWith("2")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"執行錯誤\",\r\n" +
					"    \"shortMsg\":\"執行錯誤\",\r\n" +
					"    \"resultCode\":\"099\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API1328_20200221_92FCAF3195_T01\",\r\n" +
					"  \"hostname\":\"cxlcsxat01\"\r\n" +
					"}";
		}else {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"\",\r\n" +
					"    \"shortMsg\":\"查詢成功\",\r\n" +
					"    \"resultCode\":\"000\",\r\n" +
					"    \"data\":{\r\n" +
					"      \"BIRTHDAY\":\"0741222\",\r\n" +
					"      \"LOCAL_MEDICAL\":\"0\",\r\n" +
					"      \"AGE\":\"34\",\r\n" +
					"      \"IS_EMPLOYEE\":\"Y\",\r\n" +
					"      \"AGENT_QUALIFICATION\":\"Y\",\r\n" +
					"      \"OVERSEA_MEDICAL\":\"10\",\r\n" +
					"      \"OVERSEA_ILLNESS_TYPE\":\"3\",\r\n" +
					"      \"IS_MEMBER\":\"Y\",\r\n" +
					"      \"CREDITCARD_VALIDDATE\":\"202010\",\r\n" +
					"      \"LOCAL_MAIN\":\"100\",\r\n" +
					"      \"MEMBER_TYPE\":\"MAIN\",\r\n" +
					"      \"OVERSEA_ILLNESS\":\"20\",\r\n" +
					"      \"OVERSEA_MAIN\":\"100\"\r\n" +
					"    },\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":0\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API1328_20200221_866D181AD1_T01\",\r\n" +
					"  \"hostname\":\"cxlcsxat01\"\r\n" +
					"}"
					;
		}
		return result;
	}

	/**
     * {"VIP_ID":"A123456789"}
     *
     * VIP_ID
     * _尾碼0 -> 傳入參數有誤
     * _尾碼1 -> 檢核特殊名單
     * _尾碼2 -> 查無會員資料
     * _尾碼3 -> 查詢正常:整戶資料只有一個主會員
     * _尾碼4 -> 檢核經手人有誤
     * _尾碼5 -> 系統有誤
     * _其他  -> 查詢正常:整戶資料有主會員、從屬會員
     *
     */
	@RequestMapping(value = "/all_vip_data",
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String allVipData(@RequestBody Map<String, String> param) {
		String vipId = param.get("VIP_ID");
		String result = null;

		if(vipId.endsWith("0")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"傳入參數:會員 ID 格式有誤\",\r\n" +
					"    \"shortMsg\":\"未通過輸入檢核\",\r\n" +
					"    \"resultCode\":\"097\",\r\n" +
					"    \"data\":\"A12345\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200130_74C2D87F54_S02\",\r\n" +
					"  \"hostname\":\"cxl1csas01\"\r\n" +
					"}"
					;
		} else if(vipId.endsWith("1")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"人壽不受歡迎名單,會員 id:A29091346D;\",\r\n" +
					"    \"shortMsg\":\"檢核特殊名單\",\r\n" +
					"    \"resultCode\":\"002\",\r\n" +
					"    \"data\":{\r\n" +
					"      \"notWelcome\":\"01\"\r\n" +
					"    },\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":0\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200309_267398BDC8_S02\",\r\n" +
					"  \"hostname\":\"cxl1csas01\"\r\n" +
					"}"
					;
		} else if(vipId.endsWith("2")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"\",\r\n" +
					"    \"shortMsg\":\"查無會員資料\",\r\n" +
					"    \"resultCode\":\"098\",\r\n" +
					"    \"data\":\"A123456789\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200130_74C2D87F54_S02\",\r\n" +
					"  \"hostname\":\"cxl1csas01\"\r\n" +
					"}"
					;
		} else if(vipId.endsWith("3")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"\",\r\n" +
					"    \"shortMsg\":\"查詢成功\",\r\n" +
					"    \"resultCode\":\"000\",\r\n" +
					"    \"data\":{\r\n" +
					"      \"checkAGNT_ID\":\"00\",\r\n" +
					"      \"onlyVIP\":\"N\",\r\n" +
					"      \"newCASE_NO\":\"N\",\r\n" +
					"      \"a190_bo\":{\r\n" +
					"        \"case_no\":\"A000141317\",\r\n" +
					"        \"vip_id\":\"N24477208K\",\r\n" +
					"        \"vip_name\":\"壽六科\",\r\n" +
					"        \"vip_brdy\":\"1991-01-01\",\r\n" +
					"        \"mobile\":\"0973303179\",\r\n" +
					"        \"zip_code\":\"114\",\r\n" +
					"        \"addr\":\"陽光街 288 號\",\r\n" +
					"        \"tel_area\":\"02\",\r\n" +
					"        \"tel\":\"86867474\",\r\n" +
					"        \"tel_ext\":\"\",\r\n" +
					"        \"email\":\"abc@gmail.com\",\r\n" +
					"        \"aply_date\":\"2019-12-05\",\r\n" +
					"        \"card_no\":\"5430450130000009\",\r\n" +
					"        \"card_name_ym\":\"202812\",\r\n" +
					"        \"card_kind\":\"1\",\r\n" +
					"        \"agnt_id\":\"N24477208K\",\r\n" +
					"        \"agnt_name\":\"劉○蘭\",\r\n" +
					"        \"agnt_mobile\":\"0982123456\",\r\n" +
					"        \"agnt_div_no\":\"9200900\",\r\n" +
					"        \"input_id\":\"N24477208K\",\r\n" +
					"        \"input_div_no\":\"9200900\",\r\n" +
					"        \"input_name\":\"劉○蘭\",\r\n" +
					"        \"vip_type\":\"1\",\r\n" +
					"        \"input_date\":\"2019-12-05 16:14:12.772\",\r\n" +
					"        \"agnt_tel_area\":\"02\",\r\n" +
					"        \"agnt_tel\":\"26566999\",\r\n" +
					"        \"agnt_tel_ext\":\"2866\",\r\n" +
					"        \"aply_sts\":\"1\",\r\n" +
					"        \"agnt_licence\":\"0096201770\",\r\n" +
					"        \"national\":\"TW\",\r\n" +
					"        \"national_memo\":\"中華民國\",\r\n" +
					"        \"risk_occu_code\":\"J800\",\r\n" +
					"        \"risk_jobtitle_code\":\"T010\",\r\n" +
					"        \"m_retMsg\":{\r\n" +
					"          \"m_desc\":[\r\n" +
					"            \"\"\r\n" +
					"          ],\r\n" +
					"          \"m_msgid\":\"\",\r\n" +
					"          \"m_sysid\":\"\",\r\n" +
					"          \"m_type\":\"\",\r\n" +
					"          \"m_url\":\"\",\r\n" +
					"          \"m_idx\":0,\r\n" +
					"          \"m_code\":0\r\n" +
					"        }\r\n" +
					"      },\r\n" +
					"      \"PROD_ID\":\"Y\",\r\n" +
					"      \"notWelcome\":\"00\",\r\n" +
					"      \"newCASE_NO_INS\":\"N\",\r\n" +
					"      \"TRVL_PROD_ID\":\"N\"\r\n" +
					"    },\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":0\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200309_57B0666F66_S01\",\r\n" +
					"  \"hostname\":\"cxl1csas02\"\r\n" +
					"}"
					;
		} else if(vipId.endsWith("4")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"檢核經手人有誤\",\r\n" +
					"    \"shortMsg\":\"檢核經手人有誤\",\r\n" +
					"    \"resultCode\":\"001\",\r\n" +
					"    \"data\":\"A12345\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200130_74C2D87F54_S02\",\r\n" +
					"  \"hostname\":\"cxl1csas01\"\r\n" +
					"}"
					;
		} else if(vipId.endsWith("5")) {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"系統有誤\",\r\n" +
					"    \"shortMsg\":\"系統有誤\",\r\n" +
					"    \"resultCode\":\"099\",\r\n" +
					"    \"data\":\"A12345\",\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":-3\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200130_74C2D87F54_S02\",\r\n" +
					"  \"hostname\":\"cxl1csas01\"\r\n" +
					"}"
					;
		} else {
			result =
					"{\r\n" +
					"  \"returnCode\":\"0\",\r\n" +
					"  \"detail\":{\r\n" +
					"    \"resultMsg\":\"\",\r\n" +
					"    \"shortMsg\":\"查詢成功\",\r\n" +
					"    \"resultCode\":\"000\",\r\n" +
					"    \"data\":{\r\n" +
					"      \"checkAGNT_ID\":\"00\",\r\n" +
					"      \"onlyVIP\":\"N\",\r\n" +
					"      \"newCASE_NO\":\"N\",\r\n" +
					"      \"a191_boList\":[\r\n" +
					"        {\r\n" +
					"          \"case_no\":\"A030748397\",\r\n" +
					"          \"vip_id\":\"H29376308F\",\r\n" +
					"          \"rela_id\":\"A13042355K\",\r\n" +
					"          \"rela_name\":\"傅○熙\",\r\n" +
					"          \"rela_brdy\":\"2008-05-28\",\r\n" +
					"          \"mobile\":\"0922123456\",\r\n" +
					"          \"rela_kind\":\"4\",\r\n" +
					"          \"law_name\":\"\",\r\n" +
					"          \"law_rela\":\"\",\r\n" +
					"          \"rela_type\":\"1\",\r\n" +
					"          \"national\":\"TW\",\r\n" +
					"          \"case_no_ins\":\"E0810236241\",\r\n" +
					"          \"national_memo\":\"中華民國\",\r\n" +
					"          \"risk_occu_code\":\"J800\",\r\n" +
					"          \"risk_jobtitle_code\":\"T010\",\r\n" +
					"          \"m_retMsg\":{\r\n" +
					"            \"m_desc\":[\r\n" +
					"              \"\"\r\n" +
					"            ],\r\n" +
					"            \"m_msgid\":\"\",\r\n" +
					"            \"m_sysid\":\"\",\r\n" +
					"            \"m_type\":\"\",\r\n" +
					"            \"m_url\":\"\",\r\n" +
					"            \"m_idx\":0,\r\n" +
					"            \"m_code\":0\r\n" +
					"          }\r\n" +
					"        },\r\n" +
					"        {\r\n" +
					"          \"case_no\":\"A030748397\",\r\n" +
					"          \"vip_id\":\"H29376308F\",\r\n" +
					"          \"rela_id\":\"H19316816N\",\r\n" +
					"          \"rela_name\":\"謝配偶\",\r\n" +
					"          \"rela_brdy\":\"1971-08-08\",\r\n" +
					"          \"mobile\":\"\",\r\n" +
					"          \"rela_kind\":\"1\",\r\n" +
					"          \"law_name\":\"\",\r\n" +
					"          \"law_rela\":\"\",\r\n" +
					"          \"rela_type\":\"1\",\r\n" +
					"          \"national\":\"TW\",\r\n" +
					"          \"case_no_ins\":\"E0810236241\",\r\n" +
					"          \"national_memo\":\"中華民國\",\r\n" +
					"          \"risk_occu_code\":\"J800\",\r\n" +
					"          \"risk_jobtitle_code\":\"T010\",\r\n" +
					"          \"m_retMsg\":{\r\n" +
					"            \"m_desc\":[\r\n" +
					"              \"\"\r\n" +
					"            ],\r\n" +
					"            \"m_msgid\":\"\",\r\n" +
					"            \"m_sysid\":\"\",\r\n" +
					"            \"m_type\":\"\",\r\n" +
					"            \"m_url\":\"\",\r\n" +
					"            \"m_idx\":0,\r\n" +
					"            \"m_code\":0\r\n" +
					"          }\r\n" +
					"        }\r\n" +
					"      ],\r\n" +
					"      \"a190_bo\":{\r\n" +
					"        \"case_no\":\"A030748397\",\r\n" +
					"        \"vip_id\":\"H29376308F\",\r\n" +
					"        \"vip_name\":\"謝○玲\",\r\n" +
					"        \"vip_brdy\":\"1974-05-02\",\r\n" +
					"        \"mobile\":\"0922123456\",\r\n" +
					"        \"zip_code\":\"116\",\r\n" +
					"        \"addr\":\"興隆路二段 203 巷 52 號 1\",\r\n" +
					"        \"tel_area\":\"02\",\r\n" +
					"        \"tel\":\"0227551399\",\r\n" +
					"        \"email\":\"XXXXX@cathlife.com.tw\",\r\n" +
					"        \"aply_date\":\"2019-01-16\",\r\n" +
					"        \"card_no\":\"4284306866737233\",\r\n" +
					"        \"card_name_ym\":\"202206\",\r\n" +
					"        \"card_kind\":\"1\",\r\n" +
					"        \"agnt_id\":\"A17224802A\",\r\n" +
					"        \"agnt_name\":\"謝○玲\",\r\n" +
					"        \"agnt_mobile\":\"0922123456\",\r\n" +
					"        \"agnt_div_no\":\"A819213\",\r\n" +
					"        \"input_id\":\"T25969399A\",\r\n" +
					"        \"input_div_no\":\"9P06201\",\r\n" +
					"        \"input_name\":\"顏○庭\",\r\n" +
					"        \"vip_type\":\"1\",\r\n" +
					"        \"input_date\":\"2019-01-16 14:43:24.25\",\r\n" +
					"        \"aply_sts\":\"1\",\r\n" +
					"        \"agnt_licence\":\"0111100932\",\r\n" +
					"        \"case_no_ins\":\"E0810236241\",\r\n" +
					"        \"agnt_license_ins\":\"A15E583160\",\r\n" +
					"        \"national\":\"7\",\r\n" +
					"        \"national_memo\":\"中華民國\",\r\n" +
					"        \"risk_occu_code\":\"J800\",\r\n" +
					"        \"risk_jobtitle_code\":\"T010\",\r\n" +
					"        \"m_retMsg\":{\r\n" +
					"          \"m_desc\":[\r\n" +
					"            \"\"\r\n" +
					"          ],\r\n" +
					"          \"m_msgid\":\"\",\r\n" +
					"          \"m_sysid\":\"\",\r\n" +
					"          \"m_type\":\"\",\r\n" +
					"          \"m_url\":\"\",\r\n" +
					"          \"m_idx\":0,\r\n" +
					"          \"m_code\":0\r\n" +
					"        }\r\n" +
					"      },\r\n" +
					"      \"PROD_ID\":\"Y\",\r\n" +
					"      \"notWelcome\":\"00\",\r\n" +
					"      \"newCASE_NO_INS\":\"N\",\r\n" +
					"      \"TRVL_PROD_ID\":\"N\"\r\n" +
					"    },\r\n" +
					"    \"returnMessage\":{\r\n" +
					"      \"m_desc\":[\r\n" +
					"        \"\"\r\n" +
					"      ],\r\n" +
					"      \"m_msgid\":\"\",\r\n" +
					"      \"m_sysid\":\"\",\r\n" +
					"      \"m_type\":\"\",\r\n" +
					"      \"m_url\":\"\",\r\n" +
					"      \"m_idx\":0,\r\n" +
					"      \"m_code\":0\r\n" +
					"    }\r\n" +
					"  },\r\n" +
					"  \"returnMsg\":\"API success\",\r\n" +
					"  \"UUID\":\"CB_API0467_20200309_2BBE27FBC0_S01\",\r\n" +
					"  \"hostname\":\"cxl1csas02\"\r\n" +
					"}"
					;
		}
		return result;
	}


	/**
	 * parameter -> {"APLY_NO":"0010700001"}
	 *
	 * 問問代碼    國壽API代碼    (TPI GW response的content.RESULT_CODE欄位) 說明
     * -----------------------------------------------------------------------
     * 000       000          投保成功
     * 151       097          傳入參數有誤
     * 152       098          查無核保試算資料
     * 153       099          執行錯誤
	 */
	@RequestMapping(value = "/doConfirm_voice",
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String doConfirmVoice(@RequestBody Map<String, String> param) {
	    String aplyNo = param.get("APLY_NO");

	    int returnCode = -1;
	    String shortMsg = "";
	    String resultCode = null;
	    String resultMsg = null;
	    if (aplyNo == null || aplyNo.endsWith("97")) {
	        resultCode = "097";
	        resultMsg = "傳入參數有誤";
	    } else if (aplyNo.endsWith("98")) {
	        resultCode = "098";
            resultMsg = "查無核保試算資料";
	    } else if (aplyNo.endsWith("99")) {
            resultCode = "099";
            resultMsg = "執行錯誤";
        } else {
            returnCode = 0;
            shortMsg = "契約成立";
            resultCode = "000";
            resultMsg = "正常";
        }
        return "{\r\n" +
                "    \"returnCode\":\"" + returnCode + "\",\r\n" +
                "    \"detail\":{\r\n" +
                "        \"resultMsg\":\"" + resultMsg + "\",\r\n" +
                "        \"shortMsg\":\"" + shortMsg + "\",\r\n" +
                "        \"resultCode\":\"" + resultCode + "\",\r\n" +
                "        \"data\":{\r\n" +
                "            \"APLY_NO\":\"" + aplyNo + "\"\r\n" +
                "        },\r\n" +
                "        \"returnMessage\":{\r\n" +
                "            \"m_desc\":[\r\n" +
                "                \"\"\r\n" +
                "            ],\r\n" +
                "            \"m_msgid\":\"\",\r\n" +
                "            \"m_sysid\":\"\",\r\n" +
                "            \"m_type\":\"\",\r\n" +
                "            \"m_url\":\"\",\r\n" +
                "            \"m_idx\":0,\r\n" +
                "            \"m_code\":0\r\n" +
                "        }\r\n" +
                "    },\r\n" +
                "    \"returnMsg\":\"API success\",\r\n" +
                "    \"UUID\":\"CB_API1330_20200318_620B18880A_T01\",\r\n" +
                "    \"hostname\":\"cxlcsxat01\"\r\n" +
                "}";
    }

}
