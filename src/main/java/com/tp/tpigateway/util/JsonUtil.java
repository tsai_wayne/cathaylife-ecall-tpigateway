package com.tp.tpigateway.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tp.tpigateway.vo.cathay.LawAgeData;

import okhttp3.Response;

// wayne
// 20191228
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

	private static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
//		mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES);

//		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
//		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
//		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	private static final TypeReference<Map<String, Object>> MAP_TYPE =
	        new TypeReference<Map<String, Object>>() {};

	public static String mapToJsonString(Map<String, Object> map) {
		try {
			return mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return String.format("\"error\":\"%s\"", e.getMessage());
		}
	}

	public static Map<String, Object> jsonStringToMap(String json) {
		try {
			return mapper.readValue(json, MAP_TYPE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new HashMap<String, Object>() {{
				put("error", e.getMessage());
			}};
		}
	}

	public static Map<String, Object> getMapFromResponse(Response response) {
		try {
			return jsonStringToMap(response.body().string());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new HashMap<String, Object>() {{
				put("error", e.getMessage());
			}};
		}
	}

	// wayne
	// 20191231
	public static JsonNode getItemFromKey(Object obj, String key) {
		JsonNode node = mapper.valueToTree(obj);
		return node.findValue(key);
	}

	// wayne
	// 20191231
	public static List<Map<String, Object>> ListMapFromKey(Object obj, String key) {
		JsonNode json = getItemFromKey(obj, key);
		List<Map<String, Object>> result = new ArrayList<>();
		json.forEach(j -> {
			result.add(
				mapper.convertValue(j, new TypeReference<Map<String, Object>>(){}));
		});

		return result;
	}

	public static String objectToJson(Object obj) {
        String json = null;
        try {
            json = mapper.writeValueAsString(obj);
        } catch (Exception ex) {
            return String.format("\"error\":\"%s\"", ex.getMessage());
        }
        return json;
    }

    public static <T> T jsonToObject(String json, Class<T> clazz) {
        T t = null;
        ObjectMapper om = new ObjectMapper();
        try {
            t = om.readValue(json, new TypeReference<T>(){});
            JavaType type = om.getTypeFactory().constructType(clazz);
            t = om.readValue(json, type);
        } catch (Exception ex) {
            LOGGER.error("Json to object(" + clazz.getName() + ") fail.", ex);
            throw new RuntimeException(ex);
        }
        return t;
    }

    public static Map<String, Object> objectToMap(Object obj) {
        try {
            return mapper.convertValue(obj, MAP_TYPE);
        } catch (Exception ex) {
            LOGGER.error("Object to Map fail.", ex);
            throw new RuntimeException(ex);
        }
    }

    public static <T> T mapToObject(Map<String, Object> obj, Class<T> clz) {
        try {
            return mapper.convertValue(obj, clz);
        } catch (Exception ex) {
            LOGGER.error("Map to Object fail.", ex);
            throw new RuntimeException(ex);
        }
    }

    public static <T> T jsonNodeToObject(JsonNode node, Class<T> toValueType) {
        try {
            return mapper.convertValue(node, toValueType);
        } catch (Exception ex) {
            LOGGER.error("JsonNode to Object fail.", ex);
            throw new RuntimeException(ex);
        }
    }

    public static JsonNode objectToJsonNode(Object obj) {
    	try {
            return mapper.convertValue(obj, JsonNode.class);
        } catch (Exception ex) {
            LOGGER.error("Object to JsonNode fail.", ex);
            throw new RuntimeException(ex);
        }
    }

    public static <T, R> T jsonToGenericObject(String json, Class<T> rootClass, Class<R> contentClass) throws Exception {
        T t = null;
        try {
            JavaType type = mapper.getTypeFactory().constructParametricType(rootClass, contentClass);
            t = mapper.readValue(json, type);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return t;
    }

    /**
	 * <p>檢查是否為正確的json格式</p>
	 * @date 2020年2月24日
	 * @param testString
	 * @return
	 */
//	public static boolean isJSONValid(String testString) {
//	    try {
//	        new JSONObject(testString);
//	    } catch (JSONException ex) {
//	        // edited, to include @Arthur's comment
//	        // e.g. in case JSONArray is valid as well...
//	        try {
//	            new JSONArray(testString);
//	        } catch (JSONException ex1) {
//	            return false;
//	        }
//	    }
//	    return true;
//	}
}
