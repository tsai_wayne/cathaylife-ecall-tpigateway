package com.tp.tpigateway.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tp.tpigateway.exception.TPException;
import com.tp.tpigateway.vo.flow.FlowRequest;

public class FeignCheckUtils {

	public static void checkLawAgeRequestRequired(FlowRequest req) {
		List<String> errorFields = new ArrayList<>();
		if(StringUtils.isEmpty(req.getIssueDate()) ||
		   !isCorrectDateFormat(req.getIssueDate()))
			errorFields.add("issueDate");

		for(String date:req.getVipBrdy()) {
			if(StringUtils.isEmpty(date) ||
			   !isCorrectDateFormat(date)) {
				errorFields.add("vipBrdy");
				break;
			}
		}

		if(!errorFields.isEmpty())
			throw new TPException(errorFields.toString() + " must be format yyyy-MM-dd" );
	}

	public static void checkIssueRequestRequired(FlowRequest req) {
		List<String> errorFields = new ArrayList<>();
		if(StringUtils.isEmpty(req.getVipId()))
			errorFields.add("vipId");

		if(!errorFields.isEmpty())
			throw new TPException(errorFields.toString() + " must not be empty" );
	}

	private static boolean isCorrectDateFormat(String dateString) {
		/* Check if date is 'null' */
		if (StringUtils.isEmpty(dateString))
			return false;

	    SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
	    sdfrmt.setLenient(false);
	    try {
	        sdfrmt.parse(dateString);
	        return true;
	    } catch (ParseException e) {
	        System.err.println(dateString + " is Invalid Date format");
	        return false;
	    }
	}
}
