package com.tp.tpigateway.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.tpigateway.vo.cathay.CathayBaseResponse;
import com.tp.tpigateway.vo.cathay.LawAgeData;

public class Test {

	public static <T, R> T jsonToObject(String json, Class<T> rootClass, Class<R> contentClass) throws Exception {
        ObjectMapper om = new ObjectMapper();
        T t = null;
        try {
            JavaType type = om.getTypeFactory().constructParametricType(rootClass, contentClass);
            t = om.readValue(json, type);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return t;
    }

	public static CathayBaseResponse<LawAgeData> jsonToObject(String json) throws Exception {
        ObjectMapper om = new ObjectMapper();
        CathayBaseResponse<LawAgeData> t = null;
        try {
            JavaType type = om.getTypeFactory().constructType(new TypeReference<CathayBaseResponse<LawAgeData>>() {});
            t = om.readValue(json, type);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return t;
    }

	public static void main(String[] args) throws Exception {

		String json = "{\r\n" +
				"    \"returnCode\":\"0\",\r\n" +
				"    \"detail\":{\r\n" +
				"        \"resultMsg\":\"\",\r\n" +
				"        \"shortMsg\":\"查詢成功\",\r\n" +
				"        \"resultCode\":\"000\",\r\n" +
				"        \"data\":{\r\n" +
				"            \"AGE\":\"50\",\r\n" +
				"            \"VIP_BRDY\":\"1970-01-01\",\r\n" +
				"            \"ISSUE_DATE\":\"2020-02-20\"\r\n" +
				"        },\r\n" +
				"        \"returnMessage\":{\r\n" +
				"            \"m_desc\":[\r\n" +
				"                \"\"\r\n" +
				"            ],\r\n" +
				"            \"m_msgid\":\"\",\r\n" +
				"            \"m_sysid\":\"\",\r\n" +
//				"            \"m_type\":\"\",\r\n" +
//				"            \"m_url\":\"\",\r\n" +
				"            \"m_idx\":0,\r\n" +
				"            \"m_code\":0\r\n" +
				"        }\r\n" +
				"    },\r\n" +
				"    \"returnMsg\":\"API success\",\r\n" +
				"    \"UUID\":\"CB_API1331_20200221_DEC228F4C2_T01\",\r\n" +
				"    \"hostname\":\"cxlcsxat01\"\r\n" +
				"}";
//		CathayBaseResponse<LawAgeData> res = jsonToObject(json,CathayBaseResponse.class, LawAgeData.class);
		CathayBaseResponse<LawAgeData> res = jsonToObject(json);

		String json2 = JsonUtil.objectToJson(res);
		System.out.println();
	}
}
