package com.tp.tpigateway.util;


public class FBBotResponseUtils {

//    public BotMessageVO initBotMessage(String channel, String role) {
//        BotMessageVO vo = new BotMessageVO();
//        vo.setChannel(channel);
//        vo.setRole(role);
//        vo.setContent(new ArrayList<>());
//        vo.setFbEvents(new ArrayList<>());
//        return vo;
//    }

//    public void setTextResult(BotMessageVO vo, String text) {
//        Event textEvent = getTextEvent(text);
//        vo.getFbEvents().add(textEvent);
//    }
//
//    public void setWebView(BotMessageVO vo, String url, String messageText, String btnText, WebviewHeightRatio ratio) {
//        Event webView = getWebView(url, messageText, btnText, ratio);
//        vo.getFbEvents().add(webView);
//    }
//
//    public void setWebView(BotMessageVO vo, String url, String messageText, String btnText) {
//       setWebView(vo, url, messageText, btnText, WebviewHeightRatio.full);
//    }
//
//    public void setTextAndQuickReplay(BotMessageVO vo, String text, QuickReplay... quickReplays) {
//        Event quickReplayEvent = getTextAndQuickReplayEvent(text, quickReplays);
//        vo.getFbEvents().add(quickReplayEvent);
//    }
//
//    public void setGeneric(BotMessageVO vo, List<Element> elements) {
//        Payload payload = new Payload();
//        payload.setTemplateType("generic");
//        payload.setElements(elements.toArray(new Element[elements.size()]));
//
//        Attachment attachment = new Attachment();
//        attachment.setType("template");
//        attachment.setPayload(payload);
//
//        Message message = new Message();
//        message.setAttachment(attachment);
//
//        Event event = new Event()
//                .setMessagingType("RESPONSE")
//                .setMessage(message);
//
//        vo.getFbEvents().add(event);
//    }
//
////    ************************************************************************************************
//
//    /*
//       {
//           "messaging_type": "RESPONSE",
//           "recipient": {
//               "id": "xxxxxxxxxxx"
//           },
//           "message": {
//               "text": "hello, world!"
//           }
//       }
//    */
//    public Event getTextEvent(String text) {
//        Event event = new Event()
//                .setMessagingType("RESPONSE")
//                .setMessage(new Message().setText(text));
//        return event;
//    }
//
//    public Event getTextEvent(String text, User recipient) {
//        Event event = new Event()
//                .setMessagingType("RESPONSE")
//                .setRecipient(recipient)
//                .setMessage(new Message().setText(text));
//        return event;
//    }
//
//    /*
//      {
//          "recipient":{
//            "id":"xxxxxxxxxxxxxx"
//          },
//          "messaging_type": "RESPONSE",
//          "message":{
//            "text": "Pick a color:",
//            "quick_replies":[
//              {
//                "content_type":"text",
//                "title":"Red",
//                "payload":"{'intent': '基金'}",
//                "image_url":"http://example.com/img/red.png"
//              },{
//                "content_type":"text",
//                "title":"Green",
//                "payload":"{'intent': '密碼'}",
//                "image_url":"http://example.com/img/green.png"
//              }
//            ]
//          }
//        }
//     */
//    public Event getTextAndQuickReplayEvent(String text, QuickReplay... quickReplays) {
//
//        List<Button> buttons = new ArrayList<>();
//
//        for (QuickReplay q : quickReplays) {
//
//            if (q.getAction() == 4) {
//                buttons.add(
//                        new Button()
//                                .setContentType("text")
//                                .setType("web_url")
//                                .setTitle(q.getText())
//                                .setUrl(q.getUrl())
//                );
//            } else {
//                buttons.add(
//                        new Button()
//                                .setContentType("text")
//                                .setTitle(q.getText())
//                                .setPayload(JSON.toJSONString(q))
//                );
//            }
//        }
//
//        Event event = new Event()
//                .setMessagingType("RESPONSE")
//                .setMessage(new Message()
//                        .setText(text)
//                        .setQuickReplies(buttons.toArray(new Button[buttons.size()])));
//        return event;
//    }
//
//    /*
//        {
//            "recipient": {
//                "id": "xxxxxxxxxxxxxx"
//            },
//            "message": {
//                "attachment": {
//                    "type": "template",
//                    "payload": {
//                        "template_type": "button",
//                        "text": "What do you want to do next?",
//                        "buttons": [
//                            {
//                                "type": "text",
//                                "url": "test",
//                                "title": "Visit Messenger"
//                            }
//                        ]
//                    }
//                }
//            }
//        }
//     */
//    public Event getWebView(String url, String messageText, String btnText, WebviewHeightRatio ratio) {
//        Button webViewBtn = getWebViewBtn(url, btnText, ratio);
//        Button[] buttons = new Button[]{webViewBtn};
//
//        Message message = getMessage(messageText, buttons);
//
//        Event event = new Event()
//                .setMessagingType("RESPONSE")
//                .setMessage(message);
//
//        return event;
//    }
//
//    /*
//   {
//      "recipient": {
//        "id": "2586760691372708"
//      },
//      "message": {
//        "attachment": {
//          "type": "template",
//          "payload": {
//            "template_type": "button",
//            "text": "What do you want to do next?",
//            "buttons": [
//              {
//                "type": "postback",
//                "title": "Start Chatting",
//                "payload": "DEVELOPER_DEFINED_PAYLOAD"
//              }
//            ]
//          }
//        }
//      }
//    }
//    */
//
//    public Button getPostbackBtn(QuickReplay quickReplay) {
//        return new Button()
//                .setType("postback")
//                .setTitle(quickReplay.getText())
//                .setPayload(JSON.toJSONString(quickReplay));
//    }
//
//    public Button[] getPostbackBtns(QuickReplay... quickReplays) {
//        Button[] buttons = new Button[quickReplays.length];
//        for (int i=0; i<quickReplays.length; i++) {
//            buttons[i] = getPostbackBtn(quickReplays[i]);
//        }
//        return buttons;
//    }
//
//    public Button getOpenUrlBtn(String url, String btnText) {
//        return new Button()
//                .setType("web_url")
//                .setUrl(url)
//                .setTitle(btnText);
//    }
//
//    public Button getWebViewBtn(String url, String btnText, WebviewHeightRatio ratio) {
//        return new Button()
//                .setType("web_url")
//                .setUrl(url)
//                .setTitle(btnText)
//                .setWebviewHeightRatio(ratio.name())
//                .setMessengerExtensions(true);
//    }
//
//    public Message getMessage(String messageText, Button[] buttons) {
//        Message message = new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
//                .setTemplateType("button").setText(messageText).setButtons(buttons)));
//        return message;
//    }
//
//    /*  類似牌卡
//   {
//      "recipient": {
//        "id": "2586760691372708"
//      },
//      "message": {
//        "attachment": {
//          "type": "template",
//          "payload": {
//            "template_type": "generic",
//            "elements": [
//              {
//                "title": "Welcome!",
//                "image_url": "https://petersfancybrownhats.com/company_image.png",
//                "subtitle": "We have the right hat for everyone.",
//                "default_action": {
//                  "type": "web_url",
//                  "url": "https://petersfancybrownhats.com/view?item=103",
//                  "webview_height_ratio": "tall"
//                },
//                "buttons": [
//                  {
//                    "type": "web_url",
//                    "url": "https://petersfancybrownhats.com",
//                    "title": "View Website"
//                  },
//                  {
//                    "type": "postback",
//                    "title": "Start Chatting",
//                    "payload": "DEVELOPER_DEFINED_PAYLOAD"
//                  }
//                ]
//              }
//            ]
//          }
//        }
//      }
//    }
//    */
//    //title or subtitle不能都是空
//    public Element getElement(String title, String subtitle, String imageUrl) {
//        return new Element()
//                .setTitle(title)
//                .setSubtitle(subtitle).setItemUrl(imageUrl);
//    }

}
