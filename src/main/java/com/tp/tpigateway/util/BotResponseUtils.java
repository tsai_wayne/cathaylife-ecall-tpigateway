package com.tp.tpigateway.util;

public class BotResponseUtils {

    // wayne
    // 20191230
    protected static final String THSR = "thsr";
    protected static final String CHAT_WEB = "ChatWeb";

    public enum Widget { // for type=12用
        doLogin, doNotice, doGeolocation, doSurvey
        // 時刻表選擇
        , ScheduleSelectionComponent, TimeTable, TimeTableTickPrice, StopStation,
        //記名卡
        dorcms,doSearchVip,
        //轉真人_二次開窗
        doDoubleconfirm
    }

//    public void setSurvey(BotMessageVO vo) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setWidget(Widget.doSurvey.name());
//        vo.getContent().add(content.getContent());
//    }
//
//    public BotMessageVO setLogin(BotMessageVO vo) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setWidget(Widget.doLogin.name());
//        vo.getContent().add(content.getContent());
//        return vo;
//    }
//    
//    public BotMessageVO setWidgetReturn(BotMessageVO vo,String widget) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setOnlyWidget(widget);
//        vo.getContent().add(content.getContent());
//        return vo;
//    }
//
//    public BotMessageVO setScheduleSelectionComponent(BotMessageVO vo, String dateTime) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setWidget(Widget.ScheduleSelectionComponent.name());
//        content.getContent().put("dateTime", dateTime);
//        vo.getContent().add(content.getContent());
//        return vo;
//    }
//
//    public BotMessageVO initBotMessage(String channel, String role) {
//        BotMessageVO vo = new BotMessageVO();
//        vo.setChannel(channel);
//        vo.setRole(role);
//        vo.setContent(new ArrayList<>());
//        vo.setFbEvents(new ArrayList<>());
//        return vo;
//    }
//
//    /**
//     * @author Wayne Tsai
//     * @date 2020年1月6日下午6:11:28
//     */
//    public BotMessageVO initBotMessageForThsrCard(String channel, String fbintent, String sbintent) {
//        BotMessageVO vo = new BotMessageVO();
//        vo.setChannel(channel);
//        vo.setRole(THSR);
//        vo.setSource(THSR);
//        vo.setFbintent(fbintent);
//        vo.setSbintent(sbintent);
//        vo.setContent(new ArrayList<>());
//        return vo;
//    }
//
//    public BotMessageVO initBotMessageForThsrCard(String channel) {
//        BotMessageVO vo = new BotMessageVO();
//        vo.setChannel(channel);
//        vo.setRole(THSR);
//        vo.setSource(THSR);
//        vo.setContent(new ArrayList<>());
//        return vo;
//    }
//
//    /**
//     * @author West
//     * @version 建立時間:Dec 30, 2019 23:34:37 PM 說明:給日期時刻表等使用
//     */
//    public BotMessageVO initBotMessage() {
//        BotMessageVO vo = new BotMessageVO();
//        vo.setChannel(CHAT_WEB);
//        vo.setRole(THSR);
//        vo.setSource(THSR);
//        vo.setContent(new ArrayList<>());
//        return vo;
//    }
//    
//    public PgsmsMessageVO initPgsmsMessage() {
//        PgsmsMessageVO vo = new PgsmsMessageVO();
//        vo.setChannel(CHAT_WEB);
//        vo.setRole(THSR);
//        vo.setSource(THSR);
//        vo.setContent(new ArrayList<>());
//        return vo;
//    }
//
//    public void setTextResult(BotMessageVO vo, String text) {
//        ContentItem contentH = this.getTextContentItem(vo, text);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setNoteResult(BotMessageVO vo, String annoTitle, String annoText) {
//        ContentItem contentH = this.getNoteContentItem(vo, annoTitle, annoText);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setHNoteResult(BotMessageVO vo, String annoText) {
//        ContentItem contentH = getHNoteContentItem(vo, annoText);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setHNoteResultWithReplace(BotMessageVO vo, String annoText, Map<String, String> replaceParams) {
//        ContentItem contentH = this.getHNoteContentItemWithReplace(vo, annoText, replaceParams);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public ContentItem getHNoteContentItem(BotMessageVO vo, String annoText) {
//        ContentItem contentH = vo.new ContentItem();
//        contentH.setType(13);
//        contentH.setAnnoText(annoText);
//        return contentH;
//    }
//
//    public ContentItem getHNoteContentItemWithReplace(BotMessageVO vo, String annoText,
//                                                      Map<String, String> replaceParams) {
//        return getHNoteContentItem(vo, replaceTextByParamMap(annoText, replaceParams));
//    }
//
//    public void setNoteResultWithReplace(BotMessageVO vo, String annoTitle, String annoText,
//                                         Map<String, String> replaceParams) {
//        ContentItem contentH = this.getNoteContentItemWithReplace(vo, annoTitle, annoText, replaceParams);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setTextResultWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
//        ContentItem contentH = this.getTextContentItemWithReplace(vo, text, paramMap);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setCardsResult(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(2);
//        content.setCType(ctype);
//        content.setCards(cardList);
//        vo.getContent().add(content.getContent());
//    }
//
//    /**
//     * @author west
//     * @version 建立時間:Dec 30, 2019 23:34:37 AM 類 說明:給日期使用
//     */
//    public void setCardsResult(BotMessageVO vo, Integer action, String widget, String intent) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setAction(action);
//        content.setWidget(widget);
//        content.setIntent(intent);
//        vo.getContent().add(content.getContent());
//    }
//
//    /**
//     * @author west
//     * @version 建立時間:Dec 31, 2019 11:10:37 AM 類 說明:票價使用
//     */
//    public void setCardsResult(BotMessageVO vo, Integer action, List<Map<String, Object>> basicFare) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setAction(action);
//        content.setWidget("TimeTableTickPrice");
//        content.setBasicFare(basicFare);
//        vo.getContent().add(content.getContent());
//    }
//
//    /**
//     * @author west
//     * @version 建立時間:Dec 31, 2019 15:34:37 AM 類 說明:給日期使用
//     */
//    public void setCardsResultWithPhone(BotMessageVO vo, Integer action, String widget, String phone) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setAction(action);
//        content.setWidget(widget);
//        content.setPhone(phone);
//        vo.getContent().add(content.getContent());
//    }
//
//    /**
//     * @author west
//     * @version 建立時間:2020/1/9 11:10:37 AM 類 說明:給時刻表使用
//     */
//    public void setCardsResult(BotMessageVO vo, String opDate, String number, String direction, Integer nrCars,
//                               List<Map<String, Object>> station, List<Map<String, Object>> basicFare) {
//        ContentItem content = vo.new ContentItem();
//        content.setAction(302);
//        content.setType(12);
//        content.setWidget("TimeTable");
//        content.setOpDate(opDate);
//        content.setNumber(number);
//        content.setDirection(direction);
//        content.setNrCars(nrCars);
//        content.setStationList(station);
//        content.setBasicFare(basicFare);
//
//        vo.getContent().add(content.getContent());
//    }
//
//    public void setPicComponentResult(BotMessageVO vo, String imageUrl, Boolean isOutUrl) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(5);
//        content.setImageUrl(imageUrl);
//        content.IsOutUrl(isOutUrl);
//        vo.getContent().add(content.getContent());
//    }
//
//    public void setInsResData(BotMessageVO vo, Map<String, Object> dataMap) {
//        vo.getContent().add(dataMap);
//    }
//
//    public void setTimeTableComp(BotMessageVO vo, List<Map<String, Object>> dataList) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setIntent("時刻表");
//        content.setWidget(Widget.TimeTable.name());
//        content.setDataList(dataList);
//        vo.getContent().add(content.getContent());
//    }
//
//    public void setTimeTableTickPriceComp(BotMessageVO vo, List<Map<String, Object>> dataList) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setIntent("時刻表");
//        content.setWidget(Widget.TimeTableTickPrice.name());
//        content.setDataList(dataList);
//        vo.getContent().add(content.getContent());
//    }
//
//    public void setStopStationComp(BotMessageVO vo, List<Map<String, Object>> dataList) {
//        ContentItem content = vo.new ContentItem();
//        content.setType(12);
//        content.setWidget(Widget.StopStation.name());
//        content.setDataList(dataList);
//        vo.getContent().add(content.getContent());
//    }
//
//    public ContentItem getTextContentItem(BotMessageVO vo, String text) {
//        ContentItem contentH = vo.new ContentItem();
//        contentH.setType(1);
//        contentH.setText(text);
//        return contentH;
//    }
//
//    public ContentItem getNoteContentItem(BotMessageVO vo, String annoTitle, String annoText) {
//        ContentItem contentH = vo.new ContentItem();
//        contentH.setType(11);
//        contentH.setAnnoTitle(annoTitle);
//        contentH.setAnnoText(annoText);
//        return contentH;
//    }
//
//    public ContentItem getNoteContentItemWithReplace(BotMessageVO vo, String annoTitle, String annoText,
//                                                     Map<String, String> paramMap) {
//        return getNoteContentItem(vo, annoTitle, replaceTextByParamMap(annoText, paramMap));
//    }
//
//    public ContentItem getImageContentItem(BotMessageVO vo, String imageUrl) {
//        ContentItem contentH = vo.new ContentItem();
//        contentH.setType(5);
//        contentH.setAction(6);
//        contentH.setImageUrl(imageUrl);
//        return contentH;
//    }
//
//    public void setImageResult(BotMessageVO vo, String imageUrl) {
//        ContentItem contentH = this.getImageContentItem(vo, imageUrl);
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setGeolocationResult(BotMessageVO vo) {
//        ContentItem contentH = vo.new ContentItem();
//        contentH.setType(12);
//        contentH.setWidget(Widget.doGeolocation.name());
//        vo.getContent().add(contentH.getContent());
//    }
//
//    public void setQuickReplayResult(BotMessageVO vo, QuickReplay... quickReplay) {
//        List<QuickReplay> links = Arrays.asList(quickReplay);
//
//        int[] wordLen = new int[links.size()];
//
//        List<Map<String, Object>> linkLists = new ArrayList<>();
//        for (int i = 0; i < links.size(); i++) {
//            QuickReplay link = links.get(i);
//            LinkListItem linkItem = this.getLinkListItem(vo, link);
//            linkLists.add(linkItem.getLinkList());
//
//            wordLen[i] = getWordLen(link.getText());
//        }
//
//        String lType = "";
//        int len = links.size();
//        if (wordLen.length == 2)
//            lType = (wordLen[0] <= 9 && wordLen[1] <= 9) ? BotMessageVO.LTYPE_01 : BotMessageVO.LTYPE_04;
//        else if (len == 3)
//            lType = (wordLen[0] <= 5 && wordLen[1] <= 5 && wordLen[2] <= 5) ? BotMessageVO.LTYPE_02
//                    : BotMessageVO.LTYPE_04;
//        else if (len == 4)
//            lType = (wordLen[0] <= 9 && wordLen[1] <= 9 && wordLen[2] <= 9 && wordLen[3] <= 9) ? BotMessageVO.LTYPE_03
//                    : BotMessageVO.LTYPE_04;
//        else
//            lType = BotMessageVO.LTYPE_04;
//
//        this.setQuickReplayResult(vo, lType, quickReplay);
//    }
//
//    public void setQuickReplayResult(BotMessageVO vo, String linkType, QuickReplay... quickReplay) {
//        ContentItem contentLinkListH = vo.new ContentItem();
//        contentLinkListH.setType(4);
//
//        List<QuickReplay> links = Arrays.asList(quickReplay);
//
//        List<Map<String, Object>> linkLists = new ArrayList<>();
//        for (int i = 0; i < links.size(); i++) {
//            QuickReplay link = links.get(i);
//            LinkListItem linkItem = this.getLinkListItem(vo, link);
//            linkLists.add(linkItem.getLinkList());
//        }
//
//        contentLinkListH.setLType(linkType);
//        contentLinkListH.setLinkList(linkLists);
//
//        vo.getContent().add(contentLinkListH.getContent());
//    }
//
//    public void setQuickReplayResult(BotMessageVO vo, String linkType, List<QuickReplay> links) {
//        ContentItem contentLinkListH = vo.new ContentItem();
//        contentLinkListH.setType(4);
//
//        List<Map<String, Object>> linkLists = new ArrayList<>();
//        for (int i = 0; i < links.size(); i++) {
//            QuickReplay link = links.get(i);
//            LinkListItem linkItem = this.getLinkListItem(vo, link);
//            linkLists.add(linkItem.getLinkList());
//        }
//
//        contentLinkListH.setLType(linkType);
//        contentLinkListH.setLinkList(linkLists);
//
//        vo.getContent().add(contentLinkListH.getContent());
//    }
//
//    protected int getWordLen(String str) {
//        int chinese = 0;
//        int english = 0;
//        for (int i = 0; i < str.length(); i++) {
//            char c = str.charAt(i);
//            if ((int) c < 256)
//                english++;
//            else
//                chinese++;
//        }
//        int engLen = english / 2;
//
//        return chinese * 1 + (english % 2 == 0 ? engLen : engLen + 1);
//    }
//
//    public String replaceTextByParamMap(String text, Map<String, String> paramMap) {
//
//        if (StringUtils.isBlank(text) || !StringUtils.contains(text, "<%") || !StringUtils.contains(text, "%>"))
//            return text;
//        StringBuffer sbf = new StringBuffer();
//        String[] col_ids = text.split("<%|%>");
//        for (String col_id : col_ids) {
//            if (col_id.length() > 0 && paramMap.get(col_id) != null) {
//                sbf.append(MapUtils.getString(paramMap, col_id));
//            } else {
//                if (text.indexOf("<%" + col_id + "%>") != -1)
//                    sbf.append("<%" + col_id + "%>");
//                else
//                    sbf.append(col_id);
//            }
//        }
//        return sbf.toString();
//    }
//
//    public ContentItem getTextContentItemWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
//        return getTextContentItem(vo, replaceTextByParamMap(text, paramMap));
//    }
//
//    public LinkListItem getLinkListItem(BotMessageVO vo, QuickReplay quickReplay) {
//        LinkListItem linkList = vo.new LinkListItem();
//        linkList.setLAction(quickReplay.getAction());
//        linkList.setLText(quickReplay.getText());
//        linkList.setLAlt(quickReplay.getAlt());
//        linkList.setLReply(quickReplay.getReply());
//        linkList.setLUrl(quickReplay.getUrl());
//        linkList.setLCustomerAction(quickReplay.getCustomerAction());
//        linkList.setLPrompt(quickReplay.getPrompt());
//        linkList.setLIntent(quickReplay.getIntent());
//        linkList.setLSecondBotIntent(quickReplay.getSecondBotIntent());
//        linkList.setLParameter(quickReplay.getParameter());
//        return linkList;
//    }
//
//    public CardItem buildBaseCard(BotMessageVO vo, String cImageUrl, String title, String text, String cImage,
//                                  QuickReplay... quickReplays) {
//        CardItem cards = vo.new CardItem();
//        cards.setCName("");
//        cards.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData = vo.new CImageDataItem();
//        cImageData.setCImage(cImage);
//        cImageData.setCImageUrl(cImageUrl);
//        cards.setCImageData(cImageData.getCImageDatas());
//
//        if (StringUtils.isNotBlank(title)) {
//            cards.setCTitle(title);
//        }
//
//        if (StringUtils.isNotBlank(text)) {
//            List<Map<String, Object>> cTextList = new ArrayList<>();
//            CTextItem cText = vo.new CTextItem();
//            cText.setCText(text);
//            cTextList.add(cText.getCTexts());
//            cards.setCTexts(cTextList);
//        }
//
//        cards.setCLinkList(getCLinkList(vo, quickReplays));
//
//        return cards;
//    }
//
//    public CardItem buildBaseCard(BotMessageVO vo, String cImageUrl, String title, String text,
//                                  QuickReplay... quickReplays) {
//        return buildBaseCard(vo, cImageUrl, title, text, BotMessageVO.IMG_CC, quickReplays);
//    }
//
//    public List<Map<String, Object>> getCLinkList(BotMessageVO vo, QuickReplay... quickReplay) {
//        List<Map<String, Object>> cLinkContentList = new ArrayList<>();
//
//        List<QuickReplay> links = Arrays.asList(quickReplay);
//
//        for (QuickReplay link : links)
//            cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
//
//        return cLinkContentList;
//    }
//
//    // wayne
//    // 20191225
//    // for ThsrCardService.card
//    public List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<QuickReplay> quickReplay) {
//        List<Map<String, Object>> cLinkContentList = new ArrayList<>();
//
//        for (QuickReplay link : quickReplay)
//            if (!link.getText().equals("")) // 檢查是否有子項目
//                cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
//
//        return cLinkContentList;
//    }
//
//    public CLinkListItem getCLinkListItem(BotMessageVO vo, QuickReplay quickReplay) {
//        CLinkListItem cLinkContent = vo.new CLinkListItem();
//        cLinkContent.setClAction(quickReplay.getAction());
//        cLinkContent.setClText(quickReplay.getText());
//        cLinkContent.setClAlt(quickReplay.getAlt());
//        cLinkContent.setClPrompt(quickReplay.getPrompt());
//        
//        if (!StringUtils.isEmpty(quickReplay.getReply()))
//            cLinkContent.setClReply(quickReplay.getReply());
//        if (!StringUtils.isEmpty(quickReplay.getUrl()))
//            cLinkContent.setClUrl(quickReplay.getUrl());
//        if (!StringUtils.isEmpty(quickReplay.getIntent()))
//            cLinkContent.setClIntent(quickReplay.getIntent());
//        if (!StringUtils.isEmpty(quickReplay.getSecondBotIntent()))
//            cLinkContent.setClSecondBotIntent(quickReplay.getSecondBotIntent());
//        if (quickReplay.getParameter() != null && !quickReplay.getParameter().isEmpty())
//            cLinkContent.setClParameter(quickReplay.getParameter());
//        if (!StringUtils.isEmpty(quickReplay.getPrompt()))
//            cLinkContent.setClPrompt(quickReplay.getPrompt());
//        return cLinkContent;
//    }
//
//    public BotMessageVO getSimpleBotMessageVO(String channel, String role, String text) {
//        BotMessageVO botMsgVO = initBotMessage(channel, role);
//        setTextResult(botMsgVO, text);
//        return botMsgVO;
//    }
//
//    public BotMessageVO getAutoCompleteBotMessageVO(List<AllAutocomplete> dataList) {
//        BotMessageVO botMsgVO = initBotMessage(null, null);
//
//        List<Map<String, Object>> linkList = new ArrayList<>();
//        LinkListItem link;
//        ContentItem content = botMsgVO.new ContentItem();
//
//        content.setType(4);
//        content.setWidget("autoComplete");
//        content.setLType(BotMessageVO.LTYPE_AUTO_COMPLETE);
//
//        for (AllAutocomplete allAutocomplete : dataList) {
//            link = botMsgVO.new LinkListItem();
//            link.setLAction(2);
//            link.setLAlt(allAutocomplete.getHiddenString());
//            link.setLText(allAutocomplete.getDisplayString());
//            linkList.add(link.getLinkList());
//        }
//        content.setLinkList(linkList);
//
//        List<Map<String, Object>> contentList = new ArrayList<>();
//        contentList.add(content.getContent());
//        botMsgVO.setContent(contentList);
//        return botMsgVO;
//    }

}
