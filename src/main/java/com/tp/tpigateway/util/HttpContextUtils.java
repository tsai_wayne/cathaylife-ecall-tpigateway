package com.tp.tpigateway.util;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class HttpContextUtils {

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static String getSessionId() {
        HttpServletRequest request = getHttpServletRequest();

        String sessionId = request.getParameter("sessionId");

        if (StringUtils.isEmpty(sessionId)) {
            try {
                String body = IOUtils.toString(request.getReader());
                JSONObject jsonObject = JSON.parseObject(body);
                sessionId = (String) jsonObject.get("sessionId");
            } catch (Exception e) {
                //logger.error(e.getMessage());
            }
        }

        return sessionId;
    }
}
