package com.tp.tpigateway.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "ecallapi", url = "${ecall.api.url.prefix}")
public interface ECallFeignService {

//    @Headers({"x-ibm-client-id", "3d035f85-2d0f-4979-8219-744a174f992d"})
	@RequestMapping(value = "lawAge",
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String lawAge(@RequestHeader Map<String, String> apiHeader, Map<String, String> param);
	
	@RequestMapping(value = "last_issue_data",
	        method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String lastIssueData(@RequestHeader Map<String, String> apiHeader, Map<String, String> param);
}
